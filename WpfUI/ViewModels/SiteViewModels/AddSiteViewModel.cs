﻿using Domain.Entities;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfUI.Commands;
using WpfUI.Services.Navigation;
using WpfUI.Stores;

namespace WpfUI.ViewModels.SiteViewModels
{
    public class AddSiteViewModel : ViewModelBase
    {
        private readonly SiteStore _siteStore;
        private readonly INavigationService _navigationService;

        private Site _site;

        public Site Site => _site;

        public ICommand SubmitCommand { get; }
        public ICommand CancelCommand { get; }

        public AddSiteViewModel(SiteStore siteStore, INavigationService closeNavigationService)
        {
            _siteStore = siteStore;
            _navigationService = closeNavigationService;

            _site = new();

            SubmitCommand = new AsyncRelayCommand(Add, (ex) => ErrorMessage = ex.Message);
            CancelCommand = new NavigateCommand(closeNavigationService);
        }

        private async Task Add()
        {
            ErrorMessage = string.Empty;
            IsLoading = false;
            try
            {
                IsLoading = true;
                if (Site.Name == null)
                    throw new Exception("Veuillez remplir tous les champs du formulaire.");
                await _siteStore.Add(Site);
                await _siteStore.Load();
                _navigationService.Navigate();
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
            }
            IsLoading = false;
        }
    }
}

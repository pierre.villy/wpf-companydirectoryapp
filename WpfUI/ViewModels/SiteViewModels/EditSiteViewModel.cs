﻿using Domain.Entities;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfUI.Commands;
using WpfUI.Services.Navigation;
using WpfUI.Stores;

namespace WpfUI.ViewModels.SiteViewModels
{
    public class EditSiteViewModel : ViewModelBase
    {
        private readonly SiteStore _siteStore;
        private readonly ServiceStore _serviceStore;
        private readonly EmployeeStore _employeeStore;
        private readonly INavigationService _navigationService;

        private Site _site;

        public Site Site => _site;

        public ICommand SubmitCommand { get; }
        public ICommand CancelCommand { get; }

        public EditSiteViewModel(SiteStore siteStore, INavigationService closeNavigationService, ServiceStore serviceStore, EmployeeStore employeeStore)
        {
            _siteStore = siteStore;
            _serviceStore = serviceStore;
            _employeeStore = employeeStore;
            _navigationService = closeNavigationService;

            _site = siteStore.GetSelected();
            if (_site.Name != null)
                SubmitCommand = new AsyncRelayCommand(Update, (ex) => ErrorMessage = ex.Message);
            else
                ErrorMessage = "Sélectionner un site avant de pouvoir l'éditer";

            CancelCommand = new AsyncRelayCommand(Cancel, (ex) => ErrorMessage = ex.Message);

        }

        private async Task Update()
        {
            ErrorMessage = string.Empty;
            IsLoading = false;
            try
            {
                IsLoading = true;
                await _siteStore.Update(Site);
                await _siteStore.Load();
                await _serviceStore.Refresh();
                await _employeeStore.Refresh();


                _navigationService.Navigate();
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
            }
            IsLoading = false;
        }

        private async Task Cancel()
        {
            await _siteStore.Refresh();
            _navigationService.Navigate();
        }
    }
}

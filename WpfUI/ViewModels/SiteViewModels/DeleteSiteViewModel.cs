﻿using Domain.Entities;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfUI.Commands;
using WpfUI.Services.Navigation;
using WpfUI.Stores;

namespace WpfUI.ViewModels.SiteViewModels
{
    public class DeleteSiteViewModel : ViewModelBase
    {
        private readonly SiteStore _siteStore;
        private readonly INavigationService _navigationService;

        private Site _site;

        public Site Site => _site;

        public ICommand SubmitCommand { get; }
        public ICommand CancelCommand { get; }

        public DeleteSiteViewModel(SiteStore siteStore, INavigationService closeNavigationService)
        {
            _siteStore = siteStore;
            _navigationService = closeNavigationService;

            _site = siteStore.GetSelected();

            if(_site.Name != null)
                SubmitCommand = new AsyncRelayCommand(Delete, (ex) => ErrorMessage = ex.Message);
            else
                ErrorMessage = "Sélectionner un site avant de pouvoir le supprimer";


            CancelCommand = new NavigateCommand(closeNavigationService);
        }

        private async Task Delete()
        {
            ErrorMessage = string.Empty;
            IsLoading = false;
            try
            {
                IsLoading = true;
                await _siteStore.Delete(Site);
                await _siteStore.Load();
                _navigationService.Navigate();
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
            }
            IsLoading = false;
        }
    }
}

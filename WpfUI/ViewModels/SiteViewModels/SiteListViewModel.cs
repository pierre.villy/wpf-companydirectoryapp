﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfUI.Commands;
using WpfUI.Stores;

namespace WpfUI.ViewModels
{
    internal class SiteListViewModel : ViewModelBase
    {
        #region Properties
        private readonly SiteStore _siteStore;
        private readonly ObservableCollection<Site> _sites;
        private Site _selectedSite;

        public IEnumerable<Site> Sites => _sites;

        public Site SelectedSite
        {
            get => _selectedSite;
            set
            {
                _selectedSite = value;
                OnPropertyChanged(nameof(SelectedSite));
                _siteStore.AddToSelection(_selectedSite);
            }
        }

        #endregion

        #region Commands
        public ICommand LoadSiteCommand { get; }
        #endregion

        public SiteListViewModel(SiteStore siteStore)
        {
            _siteStore = siteStore;
            _sites = new ObservableCollection<Site>();
            _selectedSite = new();
            LoadSiteCommand = new AsyncRelayCommand(LoadSiteAsync, (ex) => ErrorMessage = ex.Message);
        }

        #region AsyncTask
        private async Task LoadSiteAsync()
        {
            ErrorMessage = string.Empty;
            IsLoading = true;
            try
            {
                // Charger les différentes données dans le viewModel
                await _siteStore.Load();
                UpdateSites(_siteStore.Sites);
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
            }
            // Après exécution
            IsLoading = false;
        }

        #endregion

        public void UpdateSites(IEnumerable<Site> sites)
        {
            _sites.Clear();
            foreach (var item in sites)
                _sites.Add(item);
        }

        public static SiteListViewModel LoadViewModel(SiteStore siteStore)
        {
            SiteListViewModel viewModel = new SiteListViewModel(siteStore);
            viewModel.LoadSiteCommand.Execute(null);
            return viewModel;
        }
    }
}

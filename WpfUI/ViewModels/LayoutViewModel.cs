﻿using Domain.Entities;
using WpfUI.Commands;
using WpfUI.Stores;

namespace WpfUI.ViewModels
{
    public class LayoutViewModel : ViewModelBase
    {
        private readonly AccountStore _accountStore;

        public NavigationBarViewModel NavigationBarViewModel { get; }
        public ToolBarViewModel ToolBarViewModel { get; }
        public ViewModelBase ContentViewModel { get; }

        public bool IsAdmin => _accountStore.IsAdmin;

        public bool IsPublic => ToolBarViewModel.SelectedItemEnum.Equals(SelectedItemEnum.Default);


        public LayoutViewModel(ViewModelBase contentViewModel, NavigationBarViewModel navigationBarViewModel, ToolBarViewModel toolBarViewModel, AccountStore accountStore)
        {
            ContentViewModel = contentViewModel;
            NavigationBarViewModel = navigationBarViewModel;
            ToolBarViewModel = toolBarViewModel;
            _accountStore = accountStore;

            _accountStore.CurrentUserChanged += OnCurrentUserChanged;

        }

        private void OnCurrentUserChanged(User user)
        {
            OnPropertyChanged(nameof(IsAdmin));
        }

        public override void Dispose()
        {
            NavigationBarViewModel.Dispose();
            ContentViewModel.Dispose();
            ToolBarViewModel.Dispose();

            _accountStore.CurrentUserChanged -= OnCurrentUserChanged;

            base.Dispose();
        }


    }
}

﻿using Domain.Entities;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfUI.Commands;
using WpfUI.Services.Navigation;
using WpfUI.Stores;

namespace WpfUI.ViewModels.UserViewModels
{
    public class DeleteUserViewModel : ViewModelBase
    {
        private readonly AccountStore _accountStore;
        private readonly INavigationService _navigationService;

        private User _user;
        public User User => _user;

        public ICommand SubmitCommand { get; }
        public ICommand CancelCommand { get; }

        public DeleteUserViewModel(AccountStore accountStore, INavigationService closeNavigationService)
        {
            _accountStore = accountStore;
            _navigationService = closeNavigationService;

            _user = accountStore.GetSelected();
            if (_user.Email != null)
                SubmitCommand = new AsyncRelayCommand(Delete, (ex) => ErrorMessage = ex.Message);
            else
                ErrorMessage = "Sélectionner un utilisateur avant de pouvoir l'éditer";

            CancelCommand = new NavigateCommand(closeNavigationService); ;
        }

        private async Task Delete()
        {
            ErrorMessage = string.Empty;
            IsLoading = false;
            try
            {
                IsLoading = true;
                await _accountStore.Delete(User);
                await _accountStore.Load();
                _navigationService.Navigate();
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
            }
            IsLoading = false;
        }
    }
}

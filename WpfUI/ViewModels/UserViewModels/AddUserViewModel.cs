﻿using Domain.Entities;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfUI.Commands;
using WpfUI.Services.Navigation;
using WpfUI.Stores;

namespace WpfUI.ViewModels.UserViewModels
{
    public class AddUserViewModel : ViewModelBase
    {
        private readonly AccountStore _accountStore;
        private readonly INavigationService _navigationService;

        private User _user;
        public User User => _user;

        public ICommand SubmitCommand => new AsyncRelayCommand(Add, (ex) => ErrorMessage = ex.Message);
        public ICommand CancelCommand { get; }

        public AddUserViewModel(AccountStore accountStore, INavigationService closeNavigationService)
        {
            _accountStore = accountStore;
            _navigationService = closeNavigationService;
            _user = new();

            CancelCommand = new NavigateCommand(closeNavigationService);
        }

        private async Task Add()
        {
            ErrorMessage = string.Empty;
            IsLoading = false;
            try
            {
                IsLoading = true;
                if (User.Email == null)
                    throw new Exception("Veuillez remplir tous les champs du formulaire.");
                await _accountStore.Add(User);
                await _accountStore.Load();
                _navigationService.Navigate();
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
            }
            IsLoading = false;
        }
    }
}

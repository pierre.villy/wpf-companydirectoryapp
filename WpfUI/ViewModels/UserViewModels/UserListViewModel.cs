﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfUI.Commands;
using WpfUI.Stores;

namespace WpfUI.ViewModels.UserViewModels
{
    public class UserListViewModel : ViewModelBase
    {
        private readonly AccountStore _accountStore;
        private readonly ObservableCollection<User> _users;
        private User _selectedUser;

        public IEnumerable<User> Users => _users;

        public User SelectedUser
        {
            get => _selectedUser;
            set
            {
                _selectedUser = value;
                OnPropertyChanged(nameof(SelectedUser));
                _accountStore.AddToSelection(_selectedUser);
            }
        }
        public UserListViewModel(AccountStore accountStore)
        {
            _accountStore = accountStore;
            _users = new();
        }

        #region Commands
        public ICommand LoadUserCommand => new AsyncRelayCommand(LoadUserAsync, (ex) => ErrorMessage = ex.Message);

        #endregion

        #region AsyncTask
        private async Task LoadUserAsync()
        {
            ErrorMessage = string.Empty;
            IsLoading = true;
            try
            {
                // Charger les différentes données dans le viewModel
                await _accountStore.Load();
                UpdateUsers(_accountStore.Users);
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
            }
            // Après exécution
            IsLoading = false;
        }

        #endregion

        public void UpdateUsers(IEnumerable<User> users)
        {
            _users.Clear();
            foreach (var item in users)
                _users.Add(item);
        }
        public static UserListViewModel LoadViewModel(AccountStore accountStore)
        {
            UserListViewModel viewModel = new UserListViewModel(accountStore);
            viewModel.LoadUserCommand.Execute(null);
            return viewModel;
        }
    }
}

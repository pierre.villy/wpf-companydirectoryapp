﻿using Application.Services;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfUI.Commands;
using WpfUI.Services.Navigation;
using WpfUI.Stores;
using static Application.Interfaces.IAuthenticationService;

namespace WpfUI.ViewModels
{
    public  class MainViewModel : ViewModelBase
    {
        private readonly NavigationStore _navigationStore;
        private readonly AccountStore _accountStore;
        private readonly ModalNavigationStore _modalNavigationStore;
        private readonly AuthenticationService _authenticationService;
        private readonly INavigationService _navigationService;

        public ViewModelBase CurrentViewModel => _navigationStore.CurrentViewModel;

        public ViewModelBase CurrentModalViewModel => _modalNavigationStore.CurrentViewModel;

        public bool IsOpen => _modalNavigationStore.IsOpen;

        private string _email;
        public string Email
        {
            get => _email;
            set
            {
                _email = value;
                OnPropertyChanged(nameof(Email));
            }
        }

        private string _password;
        public string Password
        {
            get => _password;
            set
            {
                _password = value;
                OnPropertyChanged(nameof(Password));
            }
        }

        public bool IsLoggedIn => _accountStore.IsLoggedIn;
        public bool IsAdmin => _accountStore.IsAdmin;

        public ICommand LoginCommand => new AsyncRelayCommand(Login, (ex) => ErrorMessage = ex.Message);
        public ICommand LoginAsAdminCommand => new AsyncRelayCommand(LogAsAdmin, (ex) => ErrorMessage = ex.Message);
        public ICommand CancelCommand { get; }
        public ICommand RegisterCommand => new AsyncRelayCommand(Register, (ex) => ErrorMessage = ex.Message);
        public ICommand AdminLoginCommand { get; }

        public MainViewModel(NavigationStore navigationStore, AccountStore accountStore, ModalNavigationStore modalNavigationStore, AuthenticationService authenticationService, INavigationService loginNavigationService)
        {
            _navigationStore = navigationStore;
            _accountStore = accountStore;
            _modalNavigationStore = modalNavigationStore;
            _authenticationService = authenticationService;
            _navigationService = loginNavigationService;

            AdminLoginCommand = new NavigateCommand(loginNavigationService);

            _navigationStore.CurrentViewModelChanged += OnCurrentViewModelChanged;
            _modalNavigationStore.CurrentViewModelChanged += OnCurrentModalViewModelChanged;

            _accountStore.CurrentUserChanged += OnCurrentUserChanged;
        }

        private async Task Login()
        {
            User user = new User() { Email = Email, PasswordHash = Password};

            try
            {
                User logUser = _accountStore.Login(user);
                //if (user.Authentication != true)
                    //throw new Exception();
                //else if(user.Username != "Admin")
                //    throw new Exception();
                //else
                _accountStore.CurrentUser = logUser;
            }
            catch (Exception)
            {
                ErrorMessage = "L'identifiant et/ou le mot de passe sont incorrects.";
            }
        }

        private async Task LogAsAdmin()
        {
            User user = new User() { Email = Email, PasswordHash = Password };

            try
            {
                User logUser = _accountStore.Login(user);
                //if (user.Authentication != true)
                //throw new Exception();
                //else if(user.Username != "Admin")
                //    throw new Exception();
                //else
                //_accountStore.CurrentUser = logUser;
            }
            catch (Exception)
            {
                ErrorMessage = "L'identifiant et/ou le mot de passe sont incorrects.";
            }
        }

        private async Task<RegistrationResult> Register()
        {
            try
            {
                RegistrationResult result = _authenticationService.Register(Email, Password);
                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }



        private void OnCurrentUserChanged(User user)
        {
            OnPropertyChanged(nameof(IsLoggedIn));
        }

        private void OnCurrentViewModelChanged()
        {
            OnPropertyChanged(nameof(CurrentViewModel));
        }

        private void OnCurrentModalViewModelChanged()
        {
            OnPropertyChanged(nameof(CurrentModalViewModel));
            OnPropertyChanged(nameof(IsOpen));
        }
    }
}

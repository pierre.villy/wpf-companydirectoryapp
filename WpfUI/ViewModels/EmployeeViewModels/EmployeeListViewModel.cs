﻿using Application.DTO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfUI.Commands;
using WpfUI.Services.Navigation;
using WpfUI.Stores;

namespace WpfUI.ViewModels
{
    public class EmployeeListViewModel : ViewModelBase
    {
        #region Properties
        private readonly EmployeeStore _employeeStore;
        private readonly ObservableCollection<EmployeeDTO> _employees;
        private EmployeeDTO _selectedEmployee;
        
        public IEnumerable<EmployeeDTO> Employees => _employees;

        public EmployeeDTO SelectedEmployee
        {
            get => _selectedEmployee;
            set
            {
                _selectedEmployee = value;
                OnPropertyChanged(nameof(SelectedEmployee));
                _employeeStore.AddToSelection(_selectedEmployee);
            }
        }

        #endregion

        #region Commands
        public ICommand LoadEmployeeCommand { get; }
        #endregion

        public EmployeeListViewModel(EmployeeStore employeeStore)
        {
            _employeeStore = employeeStore;
            _employees = new ObservableCollection<EmployeeDTO>();
            _selectedEmployee = new();
            LoadEmployeeCommand = new AsyncRelayCommand(LoadEmployeeAsync, (ex) => ErrorMessage = ex.Message);

            _employeeStore.EmployeeAdded += OnEmployeeAdded;
            _employeeStore.EmployeeEdited += OnEmployeeEdited;
            _employeeStore.EmployeeDeleted += OnEmployeeDeleted;
        }

        #region AsyncTask
        private async Task LoadEmployeeAsync()
        {
            ErrorMessage = string.Empty;
            IsLoading = true;
            try
            {
                // Charger les différentes données dans le viewModel
                await _employeeStore.Load();
                UpdateEmployees(_employeeStore.Employees);
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
            }
            // Après exécution
            IsLoading = false;
        }

        private void OnEmployeeAdded(EmployeeDTO employee)
        {
            _employees.Add(employee);
            UpdateEmployees(_employeeStore.Employees);
        }

        private void OnEmployeeEdited(EmployeeDTO employee)
        {
            UpdateEmployees(_employeeStore.Employees);
        }

        private void OnEmployeeDeleted(EmployeeDTO employee)
        {
            _employees.Remove(employee);
            UpdateEmployees(_employeeStore.Employees);
        }


        #endregion

        public void UpdateEmployees(IEnumerable<EmployeeDTO> employees)
        {
            _employees.Clear();
            foreach (var item in employees)
                _employees.Add(item);
        }

        public static EmployeeListViewModel LoadViewModel(EmployeeStore employeeStore)
        {
            EmployeeListViewModel viewModel = new EmployeeListViewModel(employeeStore);
            viewModel.LoadEmployeeCommand.Execute(null);
            return viewModel;
        }
    }
}

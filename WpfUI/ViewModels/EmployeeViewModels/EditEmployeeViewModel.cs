﻿using Application.DTO;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfUI.Commands;
using WpfUI.Services.Navigation;
using WpfUI.Stores;

namespace WpfUI.ViewModels.EmployeeViewModels
{
    public class EditEmployeeViewModel : ViewModelBase
    {
        private readonly EmployeeStore _employeeStore;
        private readonly ServiceStore _serviceStore;
        private readonly SiteStore _siteStore;

        private readonly EmployeeDTO _employee;

        private readonly INavigationService _navigationService;

        private List<ServiceDTO> _services;
        private List<Site> _sites;
        private Site _selectedSite;
        private ServiceDTO _selectedService;

        private List<Site> _sitesWithServices;

        public ICommand SubmitCommand { get; }
        public ICommand CancelCommand { get; }

        public ICommand SendEmployeeCommand { get; }

        public EmployeeDTO Employee => _employee;

        public IEnumerable<Site> SitesWithServices
        {
            get => _sitesWithServices;
            set
            {
                _sitesWithServices = (List<Site>)value;
                OnPropertyChanged(nameof(SitesWithServices));
            }
        }

        public IEnumerable<ServiceDTO> Services
        {
            get => _services;
            set
            {
                _services = (List<ServiceDTO>)value;
                OnPropertyChanged(nameof(Services));
            }
        }

        public IEnumerable<Site> Sites
        {
            get => _sites;
            set
            {
                _sites = (List<Site>)value;
                OnPropertyChanged(nameof(Sites));
            }
        }

        public Site SelectedSite
        {
            get => _selectedSite;
            set
            {
                _selectedSite = value;
                OnPropertyChanged(nameof(SelectedSite));
            }
        }

        public ServiceDTO SelectedService
        {
            get => _selectedService;
            set
            {
                _selectedService = value;
                OnPropertyChanged(nameof(SelectedService));
            }
        }

        public EditEmployeeViewModel(EmployeeStore employeeStore, ServiceStore serviceStore, SiteStore siteStore, INavigationService closeNavigationService)
        {
            _navigationService = closeNavigationService;
            _employeeStore = employeeStore;
            _serviceStore = serviceStore;
            _siteStore = siteStore;

            _employee = employeeStore.GetSelected();

            _sites = (List<Site>?)_siteStore.Sites;
            _services = (List<ServiceDTO>?)_serviceStore.Services;

            if (_employee.FirstName != null)
            {
                //_selectedSite = _sites.Where(s => s.Id == _employee.SiteId).FirstOrDefault();
                //_selectedService = _services.Where(s => s.Id == _employee.ServiceId).FirstOrDefault();
                SubmitCommand = new AsyncRelayCommand(Update, (ex) => ErrorMessage = ex.Message);
            }
            else
                ErrorMessage = "Sélectionner un employé avant de pouvoir l'éditer";

            CancelCommand = new AsyncRelayCommand(Cancel, (ex) => ErrorMessage = ex.Message); 
        }

        private async Task Update()
        {
            ErrorMessage = string.Empty;
            IsLoading = false;
            try
            {
                IsLoading = true;
                //Employee.SiteId = SelectedSite.Id;
                await _employeeStore.Update(Employee);
                await _employeeStore.Load();
                _navigationService.Navigate();
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
            }
            IsLoading = false;
        }

        private async Task Cancel()
        {
            await _employeeStore.Refresh();
            _navigationService.Navigate();
        }
    }
}

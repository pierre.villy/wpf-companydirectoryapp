﻿using Application.DTO;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfUI.Commands;
using WpfUI.Services.Navigation;
using WpfUI.Stores;

namespace WpfUI.ViewModels.EmployeeViewModels
{
    public class DeleteEmployeeViewModel : ViewModelBase
    {
        private readonly EmployeeStore _employeeStore;
        private readonly ServiceStore _serviceStore;
        private readonly SiteStore _siteStore;

        private readonly EmployeeDTO _employee;

        private readonly INavigationService _navigationService;

        private List<Service> _services;
        private List<Site> _sites;
        private Site _selectedSite;

        private List<Site> _sitesWithServices;

        public ICommand SubmitCommand { get; }
        public ICommand CancelCommand { get; }

        public ICommand SendEmployeeCommand { get; }

        public EmployeeDTO Employee => _employee;

        public IEnumerable<Site> SitesWithServices
        {
            get => _sitesWithServices;
            set
            {
                _sitesWithServices = (List<Site>)value;
                OnPropertyChanged(nameof(SitesWithServices));
            }
        }

        public IEnumerable<Service> Services
        {
            get => _services;
            set
            {
                _services = (List<Service>)value;
                OnPropertyChanged(nameof(Services));
            }
        }

        public IEnumerable<Site> Sites
        {
            get => _sites;
            set
            {
                _sites = (List<Site>)value;
                OnPropertyChanged(nameof(Sites));
            }
        }

        public Site SelectedSite
        {
            get => _selectedSite;
            set
            {
                _selectedSite = value;
                OnPropertyChanged(nameof(SelectedSite));
            }
        }

        public DeleteEmployeeViewModel(EmployeeStore employeeStore, ServiceStore serviceStore, SiteStore siteStore, INavigationService closeNavigationService)
        {
            _navigationService = closeNavigationService;
            _employeeStore = employeeStore;
            _serviceStore = serviceStore;
            _siteStore = siteStore;

            _employee = _employeeStore.GetSelected();

            _sites = (List<Site>?)_siteStore.Sites;
            _selectedSite = new Site();

            if (_employee.FirstName != null)
                SubmitCommand = new AsyncRelayCommand(Delete, (ex) => ErrorMessage = ex.Message);
            else
                ErrorMessage = "Sélectionner un employé avant de pouvoir le supprimer";

            CancelCommand = new NavigateCommand(closeNavigationService);
        }

        private async Task Delete()
        {
            ErrorMessage = string.Empty;
            IsLoading = false;
            try
            {
                IsLoading = true;
                Employee.SiteId = SelectedSite.Id;
                await _employeeStore.Delete(Employee);
                await _employeeStore.Load();
                _navigationService.Navigate();
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
            }
            IsLoading = false;
        }
    }
}

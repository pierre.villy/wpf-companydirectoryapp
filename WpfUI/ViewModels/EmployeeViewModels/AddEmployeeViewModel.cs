﻿using Application.DTO;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfUI.Commands;
using WpfUI.Services.Navigation;
using WpfUI.Stores;

namespace WpfUI.ViewModels.EmployeeViewModels
{
    public class AddEmployeeViewModel : ViewModelBase
    {
        private readonly EmployeeStore _employeeStore;
        private readonly ServiceStore _serviceStore;
        private readonly SiteStore _siteStore;

        private readonly EmployeeDTO _employee;

        private readonly INavigationService _navigationService;

        private List<ServiceDTO> _services;
        private List<Site> _sites;
        private Site _selectedSite;

        private List<Site> _sitesWithServices;

        public ICommand SubmitCommand { get; }
        public ICommand CancelCommand { get; }

        public ICommand SendEmployeeCommand { get; }

        public EmployeeDTO Employee => _employee;

        public IEnumerable<Site> SitesWithServices
        {
            get => _sitesWithServices;
            set
            {
                _sitesWithServices = (List<Site>)value;
                OnPropertyChanged(nameof(SitesWithServices));
            }
        }

        public IEnumerable<ServiceDTO> Services
        {
            get => _services;
            set
            {
                _services = (List<ServiceDTO>)value;
                OnPropertyChanged(nameof(Services));
            }
        }

        public IEnumerable<Site> Sites
        {
            get => _sites;
            set
            {
                _sites = (List<Site>)value;
                OnPropertyChanged(nameof(Sites));
            }
        }

        public Site SelectedSite
        {
            get => _selectedSite;
            set
            {
                _selectedSite = value;
                OnPropertyChanged(nameof(SelectedSite));
            }
        }

        public AddEmployeeViewModel(EmployeeStore employeeStore, ServiceStore serviceStore, SiteStore siteStore, INavigationService closeNavigationService)
        {
            _navigationService = closeNavigationService;
            _employeeStore = employeeStore;
            _serviceStore = serviceStore;
            _siteStore = siteStore;

            _employee = new EmployeeDTO();

            _sites = (List<Site>?)_siteStore.Sites;
            _services = (List<ServiceDTO>?)_serviceStore.Services;
            _selectedSite = new Site();

            SubmitCommand = new AsyncRelayCommand(Add, (ex) => ErrorMessage = ex.Message);
            CancelCommand = new NavigateCommand(closeNavigationService);
        }

        private async Task Add()
        {
            ErrorMessage = string.Empty;
            IsLoading = false;
            try
            {
                IsLoading = true;
                if (Employee.FirstName == null)
                    throw new Exception("Veuillez remplir tous les champs du formulaire.");
                //Employee.SiteId = SelectedSite.Id;
                await _employeeStore.Add(Employee);
                await _employeeStore.Load();
                _navigationService.Navigate();
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
            }
            IsLoading = false;
        }
    }
}

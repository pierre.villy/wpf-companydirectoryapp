﻿using System.Windows.Input;
using WpfUI.Commands;
using WpfUI.Services.Navigation;
using WpfUI.Stores;

namespace WpfUI.ViewModels
{
    public class ToolBarViewModel : ViewModelBase
    {
        private ViewModelBase _currentViewModel;
        private bool _isHome;
        public SelectedItemEnum SelectedItemEnum;

        public bool IsHome => _isHome;

        private readonly NavigationStore _navigationStore;

        public ViewModelBase CurrentViewModel { get; }

        //TODO: Add modal
        public ICommand NavigateAddItemCommand { get; }
        public ICommand NavigateEditItemCommand { get; }
        public ICommand NavigateDeleteItemCommand { get; }

        public ToolBarViewModel(NavigationStore navigationStore,
            INavigationService addEmployeeNavigationService,
            INavigationService editEmployeeNavigationService,
            INavigationService deleteEmployeeNavigationService,
            INavigationService addServiceNavigationService,
            INavigationService editServiceNavigationService,
            INavigationService deleteServiceNavigationService,
            INavigationService addSiteNavigationService,
            INavigationService editSiteNavigationService,
            INavigationService deleteSiteNavigationService,
            INavigationService addUserNavigationService,
            INavigationService editUserNavigationService,
            INavigationService deleteUserNavigationService)
        {
            _navigationStore = navigationStore;
            _navigationStore.CurrentSelectedItemEnumChanged += OnCurrentSelectedItemEnumChanged;

            SelectedItemEnum = _navigationStore.SelectedItem;
            _isHome = true;
            if (SelectedItemEnum != SelectedItemEnum.Default)
            {
                switch (SelectedItemEnum)
                {
                    case SelectedItemEnum.Employee:
                        _isHome = false;
                        NavigateAddItemCommand = new NavigateCommand(addEmployeeNavigationService);
                        NavigateEditItemCommand = new NavigateCommand(editEmployeeNavigationService);
                        NavigateDeleteItemCommand = new NavigateCommand(deleteEmployeeNavigationService);
                        break;
                    case SelectedItemEnum.Service:
                        _isHome = false;
                        NavigateAddItemCommand = new NavigateCommand(addServiceNavigationService);
                        NavigateEditItemCommand = new NavigateCommand(editServiceNavigationService);
                        NavigateDeleteItemCommand = new NavigateCommand(deleteServiceNavigationService);
                        break;
                    case SelectedItemEnum.Site:
                        _isHome = false;
                        NavigateAddItemCommand = new NavigateCommand(addSiteNavigationService);
                        NavigateEditItemCommand = new NavigateCommand(editSiteNavigationService);
                        NavigateDeleteItemCommand = new NavigateCommand(deleteSiteNavigationService);
                        break;
                    case SelectedItemEnum.User:
                        _isHome = false;
                        NavigateAddItemCommand = new NavigateCommand(addUserNavigationService);
                        NavigateEditItemCommand = new NavigateCommand(editUserNavigationService);
                        NavigateDeleteItemCommand = new NavigateCommand(deleteUserNavigationService);
                        break;
                    case SelectedItemEnum.Default:
                        _isHome = true;
                        break;
                }
            }
        }

        private void OnCurrentViewModelChanged()
        {
            _currentViewModel = _navigationStore.CurrentViewModel;
        }

        private void OnCurrentSelectedItemEnumChanged()
        {
            OnPropertyChanged(nameof(SelectedItemEnum));
        }
    }
}

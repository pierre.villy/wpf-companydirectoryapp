﻿using Application.DTO;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using WpfUI.Commands;
using WpfUI.Stores;

namespace WpfUI.ViewModels
{
    public class HomeViewModel : ViewModelBase
    {
        #region Properties

        private readonly EmployeeStore _employeeStore;
        private readonly ServiceStore _serviceStore;
        private readonly SiteStore _siteStore;
        private ObservableCollection<EmployeeDTO> _employees;

        private string filterText;

        private CollectionViewSource _employeeCollection;

        private List<ServiceDTO> _services;
        private List<Site> _sites;
        private Site _selectedSite;
        private ServiceDTO _selectedService;
        private List<Site> _sitesWithServices;

        public ICollectionView EmployeesCollectionView { get; }
        public IEnumerable<EmployeeDTO> Employees => _employees;

        public IEnumerable<Site> SitesWithServices
        {
            get => _sitesWithServices;
            set
            {
                _sitesWithServices = (List<Site>)value;
                OnPropertyChanged(nameof(SitesWithServices));
            }
        }

        public IEnumerable<ServiceDTO> Services
        {
            get => _services;
            set
            {
                _services = (List<ServiceDTO>)value;
                OnPropertyChanged(nameof(Services));
            }
        }

        public IEnumerable<Site> Sites
        {
            get => _sites;
            set
            {
                _sites = (List<Site>)value;
                OnPropertyChanged(nameof(Sites));
            }
        }

        public Site SelectedSite
        {
            get => _selectedSite;
            set
            {
                _selectedSite = value;
                OnPropertyChanged(nameof(SelectedSite));
            }
        }

        public ServiceDTO SelectedService
        {
            get => _selectedService;
            set
            {
                _selectedService = value;
                OnPropertyChanged(nameof(SelectedService));
            }
        }

        #endregion

        #region Commands
        public ICommand LoadEmployeeCommand { get; }
        public ICommand FilterOnSiteAndServiceCommand => new AsyncRelayCommand(SiteAndServiceFilters, (ex) => ErrorMessage = ex.Message);
        public ICommand ClearFilterCommand => new AsyncRelayCommand(ClearFilters, (ex) => ErrorMessage = ex.Message);
        #endregion

        public HomeViewModel(EmployeeStore employeeStore, ServiceStore serviceStore, SiteStore siteStore)
        {
            _employeeStore = employeeStore;
            _serviceStore = serviceStore;
            _siteStore = siteStore;

            _serviceFilter = string.Empty;

            _employees = new ObservableCollection<EmployeeDTO>();
            EmployeesCollectionView = CollectionViewSource.GetDefaultView(_employees);

            EmployeesCollectionView.Filter = FilterEmployees;
            EmployeesCollectionView.GroupDescriptions.Add(new PropertyGroupDescription(nameof(EmployeeDTO.SiteName)));

            LoadEmployeeCommand = new AsyncRelayCommand(LoadEmployeeAsync, (ex) => ErrorMessage = ex.Message);


            _sites = (List<Site>?)_siteStore.Sites;
            _services = (List<ServiceDTO>?)_serviceStore.Services;
            _selectedSite = new Site();
            _selectedSite.Name = string.Empty;
        }

        #region AsyncTask
        private async Task LoadEmployeeAsync()
        {
            ErrorMessage = string.Empty;
            IsLoading = true;
            try
            {
                // Charger les différentes données dans le viewModel
                await _employeeStore.Load();
                UpdateEmployees(_employeeStore.Employees);
                await _serviceStore.Load();
                await _siteStore.Load();
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
            }
            // Après exécution
            IsLoading = false;
        }

        private async Task SiteAndServiceFilters()
        {

            if (ServiceFilter == null || SelectedService?.Name == null)
                ServiceFilter = string.Empty;
            else
                ServiceFilter = SelectedService?.Name;

            if (SiteFilter == null || SelectedSite?.Name == null)
                SiteFilter = string.Empty;
            else
                SiteFilter = SelectedSite?.Name;
        }
        private async Task ClearFilters()
        {
            SelectedSite = new();
            SelectedSite.Name = string.Empty;
            SelectedService = new();
            SelectedService.Name = string.Empty;
            await SiteAndServiceFilters();
        }

        #endregion

        #region Filter

        private string _employeesFilter = string.Empty;
        public string EmployeesFilter
        {
            get => _employeesFilter;
            set
            {
                _employeesFilter = value;
                OnPropertyChanged(nameof(EmployeesFilter));
                EmployeesCollectionView.Refresh();
            }
        }

        private string _serviceFilter = string.Empty;
        public string ServiceFilter
        {
            get => _serviceFilter;
            set
            {
                _serviceFilter = value;
                EmployeesCollectionView.Refresh();
                OnPropertyChanged(nameof(ServiceFilter));
            }
        }

        public string SiteFilter
        {
            get => _selectedSite.Name;
            set
            {
                _selectedSite.Name = value;
                EmployeesCollectionView.Refresh();
                OnPropertyChanged(nameof(SiteFilter));
            }
        }

        private bool FilterEmployees(object obj)
        {
            if (obj is EmployeeDTO employee)
            {
                // Filtre sur Nom AND Site AND Service
                return employee.FullName.Contains(EmployeesFilter, StringComparison.InvariantCultureIgnoreCase) &&
                    employee.ServiceName.Contains(ServiceFilter, StringComparison.InvariantCultureIgnoreCase) &&
                    employee.SiteName.Contains(SiteFilter, StringComparison.InvariantCultureIgnoreCase);
            }

            return false;
        }
        #endregion

        public void UpdateEmployees(IEnumerable<EmployeeDTO> employees)
        {
            _employees.Clear();
            foreach (var item in employees)
                _employees.Add(item);
        }

        public static HomeViewModel LoadViewModel(EmployeeStore employeeStore, ServiceStore serviceStore, SiteStore siteStore)
        {
            HomeViewModel viewModel = new HomeViewModel(employeeStore, serviceStore, siteStore);
            viewModel.LoadEmployeeCommand.Execute(null);
            return viewModel;
        }

    }
}

﻿using Application.DTO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfUI.Commands;
using WpfUI.Stores;

namespace WpfUI.ViewModels
{
    public class ServiceListViewModel : ViewModelBase
    {
        #region Properties
        private readonly ServiceStore _serviceStore;
        private readonly ObservableCollection<ServiceDTO> _services;
        private ServiceDTO _selectedService;

        public IEnumerable<ServiceDTO> Services => _services;

        public ServiceDTO SelectedService
        {
            get => _selectedService;
            set
            {
                _selectedService = value;
                OnPropertyChanged(nameof(SelectedService));
                _serviceStore.AddToSelection(_selectedService);
            }
        }

        #endregion

        #region Commands
        public ICommand LoadServiceCommand { get; }
        public ICommand AddServiceCommand { get; }
        #endregion

        public ServiceListViewModel(ServiceStore serviceStore)
        {
            _serviceStore = serviceStore;
            _services = new ObservableCollection<ServiceDTO>();
            _selectedService = new();
            LoadServiceCommand = new AsyncRelayCommand(LoadServiceAsync, (ex) => ErrorMessage = ex.Message);
        }

        #region AsyncTask
        private async Task LoadServiceAsync()
        {
            ErrorMessage = string.Empty;
            IsLoading = true;
            try
            {
                // Charger les différentes données dans le viewModel
                await _serviceStore.Load();
                UpdateServices(_serviceStore.Services);
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
            }
            // Après exécution
            IsLoading = false;
        }

        private async Task Add()
        {
            ErrorMessage = string.Empty;
            IsLoading = true;
            try
            {
                // Charger les différentes données dans le viewModel
                await _serviceStore.Load();
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
            }
            // Après exécution
            IsLoading = false;
        }

        private async Task Update()
        {
            ErrorMessage = string.Empty;
            IsLoading = true;
            try
            {
                // Charger les différentes données dans le viewModel
                await _serviceStore.Load();
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
            }
            // Après exécution
            IsLoading = false;
        }

        private async Task Delete()
        {
            ErrorMessage = string.Empty;
            IsLoading = true;
            try
            {
                // Charger les différentes données dans le viewModel
                await _serviceStore.Load();
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
            }
            // Après exécution
            IsLoading = false;
        }

        #endregion

        public void UpdateServices(IEnumerable<ServiceDTO> services)
        {
            _services.Clear();
            foreach (var item in services)
                _services.Add(item);
        }

        public static ServiceListViewModel LoadViewModel(ServiceStore serviceStore)
        {
            ServiceListViewModel viewModel = new ServiceListViewModel(serviceStore);
            viewModel.LoadServiceCommand.Execute(null);
            return viewModel;
        }
    }
}

﻿using Application.DTO;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfUI.Commands;
using WpfUI.Services.Navigation;
using WpfUI.Stores;

namespace WpfUI.ViewModels.ServiceViewModels
{
    public class AddServiceViewModel : ViewModelBase
    {
        private readonly ServiceStore _serviceStore;
        private readonly INavigationService _navigationService;

        private readonly ServiceDTO _service;

        public ServiceDTO Service => _service;

        public ICommand SubmitCommand { get; }
        public ICommand CancelCommand { get; }

        public AddServiceViewModel(ServiceStore serviceStore, INavigationService closeNavigationService)
        {
            _serviceStore = serviceStore;
            _navigationService = closeNavigationService;

            _service = new();

            SubmitCommand = new AsyncRelayCommand(Add, (ex) => ErrorMessage = ex.Message);
            CancelCommand = new NavigateCommand(closeNavigationService);
        }

        private async Task Add()
        {
            ErrorMessage = string.Empty;
            IsLoading = false;
            try
            {
                IsLoading = true;
                if (Service.Name == null)
                    throw new Exception("Veuillez remplir tous les champs du formulaire.");
                await _serviceStore.Add(Service);
                await _serviceStore.Load();
                _navigationService.Navigate();
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
            }
            IsLoading = false;
        }
    }
}

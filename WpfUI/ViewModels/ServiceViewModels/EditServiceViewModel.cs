﻿using Application.DTO;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfUI.Commands;
using WpfUI.Services.Navigation;
using WpfUI.Stores;

namespace WpfUI.ViewModels.ServiceViewModels
{
    public class EditServiceViewModel : ViewModelBase
    {
        private readonly ServiceStore _serviceStore;
        private readonly EmployeeStore _employeeStore;
        private readonly INavigationService _navigationService;

        private readonly ServiceDTO _service;

        public ServiceDTO Service => _service;
        public ICommand SubmitCommand { get; }
        public ICommand CancelCommand { get; }

        public EditServiceViewModel(ServiceStore serviceStore, INavigationService closeNavigationService, EmployeeStore employeeStore)
        {
            _serviceStore = serviceStore;
            _employeeStore = employeeStore;
            _navigationService = closeNavigationService;

            _service = serviceStore.GetSelected();

            if (_service.Name != null)
                SubmitCommand = new AsyncRelayCommand(Update, (ex) => ErrorMessage = ex.Message);
            else
                ErrorMessage = "Sélectionner un service avant de pouvoir l'éditer";

            CancelCommand = new AsyncRelayCommand(Cancel, (ex) => ErrorMessage = ex.Message);
        }

        private async Task Update()
        {
            ErrorMessage = string.Empty;
            IsLoading = false;
            try
            {
                IsLoading = true;
                await _serviceStore.Update(Service);
                await _serviceStore.Load();
                await _employeeStore.Refresh();
                _navigationService.Navigate();
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
            }
            IsLoading = false;
        }

        private async Task Cancel()
        {
            await _serviceStore.Refresh();
            _navigationService.Navigate();
        }
    }
}

﻿using Application.DTO;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfUI.Commands;
using WpfUI.Services.Navigation;
using WpfUI.Stores;

namespace WpfUI.ViewModels.ServiceViewModels
{
    public class DeleteServiceViewModel : ViewModelBase
    {
        private readonly ServiceStore _serviceStore;
        private readonly INavigationService _navigationService;

        private readonly ServiceDTO _service;

        public ServiceDTO Service => _service;

        public ICommand SubmitCommand { get; }
        public ICommand CancelCommand { get; }

        public DeleteServiceViewModel(ServiceStore serviceStore, INavigationService closeNavigationService)
        {
            _serviceStore = serviceStore;
            _navigationService = closeNavigationService;

            _service = serviceStore.GetSelected();

            if (_service.Name != null)
                SubmitCommand = new AsyncRelayCommand(Delete, (ex) => ErrorMessage = ex.Message);
            else
                ErrorMessage = "Sélectionner un service avant de pouvoir le supprimer";

            CancelCommand = new NavigateCommand(closeNavigationService);
        }

        private async Task Delete()
        {
            ErrorMessage = string.Empty;
            IsLoading = false;
            try
            {
                IsLoading = true;
                await _serviceStore.Delete(Service);
                await _serviceStore.Load();
                _navigationService.Navigate();
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
            }
            IsLoading = false;
        }
    }
}

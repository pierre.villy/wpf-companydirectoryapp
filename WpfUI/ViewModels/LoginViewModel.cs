﻿using Domain.Entities;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfUI.Commands;
using WpfUI.Services.Navigation;
using WpfUI.Stores;

namespace WpfUI.ViewModels
{
    public class LoginViewModel : ViewModelBase
    {
        private readonly AccountStore _accountStore;
        private readonly INavigationService _navigationService;


        private string _email;
        public string Email
        {
            get => _email;
            set
            {
                _email = value;
                OnPropertyChanged(nameof(Email));
            }
        }

        private string _password;
        public string Password
        {
            get => _password;
            set
            {
                _password = value;
                OnPropertyChanged(nameof(Password));
            }
        }

        public ICommand LoginCommand => new AsyncRelayCommand(Login, (ex) => ErrorMessage = ex.Message);
        //public ICommand RegisterCommand { get; }
        //public ICommand SubmitCommand => new AsyncRelayCommand(Login, (ex) => ErrorMessage = ex.Message);
        public ICommand CancelCommand { get; }

        public LoginViewModel(AccountStore accountStore, INavigationService closeNavigationService)
        {
            _accountStore = accountStore;
            _navigationService = closeNavigationService;
            CancelCommand = new NavigateCommand(closeNavigationService);
        }

        private async Task Login()
        {
            User user = new User() { Email = Email, PasswordHash = Password };

            try
            {
                User logUser = _accountStore.Login(user);
                _accountStore.CurrentUser = logUser;
                CancelCommand.Execute(null);
            }
            catch (Exception)
            {
                ErrorMessage = "L'identifiant et/ou le mot de passe sont incorrects.";
            }
        }
    }
}

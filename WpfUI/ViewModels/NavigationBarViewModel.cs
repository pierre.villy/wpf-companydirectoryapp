﻿using Domain.Entities;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfUI.Commands;
using WpfUI.Services.Navigation;
using WpfUI.Stores;

namespace WpfUI.ViewModels
{
    public class NavigationBarViewModel : ViewModelBase
    {
        private readonly AccountStore _accountStore;
        private readonly NavigationStore _navigationStore;
        private readonly INavigationService _navigationService;

        public Action UserConnected;

        public ICommand NavigateHomeCommand { get; }
        public ICommand NavigateEmployeeCommand { get; }
        public ICommand NavigateServiceCommand { get; }
        public ICommand NavigateSiteCommand { get; }
        public ICommand NavigateUserCommand { get; }
        public ICommand NavigateLoginCommand { get; }
        public ICommand LogoutCommand { get; }
        
        public bool IsAdmin => _accountStore.IsAdmin;

        public SelectedItemEnum SelectedItemEnum
        {
            get => _navigationStore.SelectedItem;
            set
            {
                _navigationStore.SelectedItem = value;
                OnPropertyChanged(nameof(SelectedItemEnum));
            }
        }

        public NavigationBarViewModel(AccountStore accountStore,
            NavigationStore navigationStore,
            INavigationService homeNavigationService,
            INavigationService employeeNavigationService,
            INavigationService serviceNavigationService,
            INavigationService siteNavigationService,
            INavigationService userNavigationService,
            INavigationService loginNavigationService)
        {
            _accountStore = accountStore;
            _navigationStore = navigationStore;
            _navigationService = homeNavigationService;

            NavigateHomeCommand = new NavigateCommand(homeNavigationService, navigationStore);
            NavigateEmployeeCommand = new NavigateCommand(employeeNavigationService, navigationStore);
            NavigateServiceCommand = new NavigateCommand(serviceNavigationService, navigationStore);
            NavigateSiteCommand = new NavigateCommand(siteNavigationService, navigationStore);
            NavigateUserCommand = new NavigateCommand(userNavigationService, navigationStore);

            NavigateLoginCommand = new NavigateCommand(loginNavigationService);

            LogoutCommand = new AsyncRelayCommand(Logout, (ex) => ErrorMessage = ex.Message);

            _accountStore.CurrentUserChanged += OnCurrentUserChanged;

        }

        private async Task Logout()
        {
            _accountStore.Logout();
            _navigationService.Navigate();
        }

        private void OnCurrentUserChanged(User user)
        {
            OnPropertyChanged(nameof(IsAdmin));
        }

        public override void Dispose()
        {
            _accountStore.CurrentUserChanged -= OnCurrentUserChanged;

            base.Dispose();
        }
    }
}

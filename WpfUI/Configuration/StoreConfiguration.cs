﻿using Microsoft.Extensions.DependencyInjection;
using WpfUI.Stores;

namespace WpfUI.Configuration
{
    public class StoreConfiguration
    {
        public static IServiceCollection AddStoreConfiguration()
        {
            IServiceCollection services = new ServiceCollection();

            services.AddSingleton<AccountStore>();
            services.AddSingleton<EmployeeStore>();
            services.AddSingleton<ServiceStore>();
            services.AddSingleton<SiteStore>();
            services.AddSingleton<NavigationStore>();
            services.AddSingleton<ModalNavigationStore>();

            return services;
        }
    }
}

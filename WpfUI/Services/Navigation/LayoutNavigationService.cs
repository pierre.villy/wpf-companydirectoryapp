﻿using System;
using WpfUI.Stores;
using WpfUI.ViewModels;

namespace WpfUI.Services.Navigation
{
    public class LayoutNavigationService<TViewModel> : INavigationService where TViewModel : ViewModelBase
    {
        private readonly NavigationStore _navigationStore;
        private readonly AccountStore _accountStore;
        private readonly Func<TViewModel> _createViewModel;
        private readonly Func<NavigationBarViewModel> _createNavigationBarViewModel;
        private readonly Func<ToolBarViewModel> _createToolBarViewModel;

        public LayoutNavigationService(NavigationStore navigationStore,
            Func<TViewModel> createViewModel,
            Func<NavigationBarViewModel> createNavigationBarViewModel,
            Func<ToolBarViewModel> createToolBarViewModel,
            AccountStore accountStore)
        {
            _navigationStore = navigationStore;
            _accountStore = accountStore;
            _createViewModel = createViewModel;
            _createNavigationBarViewModel = createNavigationBarViewModel;
            _createToolBarViewModel = createToolBarViewModel;
        }

        public void Navigate()
        {
            _navigationStore.CurrentViewModel = new LayoutViewModel(
                _createViewModel(),
                _createNavigationBarViewModel(),
                _createToolBarViewModel(),
                _accountStore);
        }
    }
}

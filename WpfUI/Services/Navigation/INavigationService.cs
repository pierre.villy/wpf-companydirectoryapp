﻿namespace WpfUI.Services.Navigation
{
    public interface INavigationService
    {
        void Navigate();
    }
}

﻿using Application.DTO;
using Application.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WpfUI.Stores
{
    public class ServiceStore
    {
        private readonly List<ServiceDTO> _services;
        private Lazy<Task> _initializeLazy;
        private readonly ServiceService _serviceService;
        private ServiceDTO _selectedService;

        public IEnumerable<ServiceDTO> Services => _services;

        public event Action<ServiceDTO> ServiceAdded;
        public event Action<ServiceDTO> ServiceEdited;
        public event Action<ServiceDTO> ServiceDeleted;


        public ServiceStore(ServiceService serviceService)
        {
            _serviceService = serviceService;
            _initializeLazy = new Lazy<Task>(Initialize);
            _selectedService = new();

            _services = new List<ServiceDTO>();
        }

        public async Task Load()
        {
            try
            {
                await _initializeLazy.Value;
            }
            catch (Exception)
            {
                _initializeLazy = new Lazy<Task>(Initialize);
                throw;
            }
        }
        public async Task Refresh()
        {
            await Initialize();
        }

        private async Task Initialize()
        {
            IEnumerable<ServiceDTO> services = _serviceService.GetAll();

            _services.Clear();
            _services.AddRange(services);
        }

        #region Methods

        public void AddToSelection(ServiceDTO service)
        {
            _selectedService = service;
        }

        public ServiceDTO GetSelected()
        {
            return _selectedService;
        }

        public async Task Add(ServiceDTO serviceDTO)
        {
            _serviceService.Add(serviceDTO);
            await Initialize();
            OnServiceAdded(serviceDTO);
        }

        public async Task Update(ServiceDTO serviceDTO)
        {
            _serviceService.Update(serviceDTO);
            await Initialize();
            OnServiceEdited(serviceDTO);
        }

        public async Task Delete(ServiceDTO serviceDTO)
        {
            _serviceService.Delete(serviceDTO.Id);
            await Initialize();
            OnServiceDeleted(serviceDTO);
        }
        #endregion

        #region Handle Event
        private void OnServiceAdded(ServiceDTO service)
        {
            ServiceAdded?.Invoke(service);
        }

        private void OnServiceEdited(ServiceDTO service)
        {
            ServiceEdited?.Invoke(service);
        }

        private void OnServiceDeleted(ServiceDTO service)
        {
            ServiceDeleted?.Invoke(service);
        }
        #endregion
    }
}

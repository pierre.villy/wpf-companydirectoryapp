﻿using System;
using WpfUI.Commands;
using WpfUI.ViewModels;

namespace WpfUI.Stores
{
    public class NavigationStore
    {
        private ViewModelBase _currentViewModel;
        private SelectedItemEnum _selectedItemEnum;

        public ViewModelBase CurrentViewModel
        {
            get => _currentViewModel;
            set
            {
                _currentViewModel = value;
                OnCurrentViewdModelChanged();
            }
        }

        public SelectedItemEnum SelectedItem
        {
            get => _selectedItemEnum;
            set
            {
                _selectedItemEnum = value;
                OnCurrentSelectedItemEnumChanged();
            }
        }


        public event Action CurrentViewModelChanged;
        public event Action CurrentSelectedItemEnumChanged;

        private void OnCurrentViewdModelChanged()
        {
            CurrentViewModelChanged?.Invoke();
        }

        private void OnCurrentSelectedItemEnumChanged()
        {
            CurrentSelectedItemEnumChanged?.Invoke();
        }
    }
}

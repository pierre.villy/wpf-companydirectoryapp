﻿using Application.Services;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WpfUI.Stores
{
    public class AccountStore
    {
        private readonly AuthenticationService _authenticationService;
        private readonly List<User> _users;
        private User _selectedUser;
        private Lazy<Task> _initializeLazy;

        public IEnumerable<User> Users => _users;

        public event Action<User> UserAdded;
        public event Action<User> UserEdited;
        public event Action<User> UserDeleted;

        private User _currentUser;
        public User CurrentUser
        {
            get => _currentUser;
            set
            {
                _currentUser = value;
                CurrentUserChanged.Invoke(_currentUser);
            }
        }

        public bool IsLoggedIn => CurrentUser != null;

        public bool IsAdmin
        {
            get
            {
                if (CurrentUser != null)
                    return CurrentUser.IsAdmin;
                return false;
            }
        }

        public event Action<User> CurrentUserChanged;

        public AccountStore(AuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
            _initializeLazy = new Lazy<Task>(Initialize);
            _selectedUser = new();

            _users = new();
        }

        #region Init Data
        public async Task Load()
        {
            try
            {
                await _initializeLazy.Value;
            }
            catch (Exception)
            {
                _initializeLazy = new Lazy<Task>(Initialize);
                throw;
            }
        }
        public async Task Refresh()
        {
            await Initialize();
        }
        private async Task Initialize()
        {
            IEnumerable<User> users = _authenticationService.GetAll();

            _users.Clear();
            _users.AddRange(users);
        }
        #endregion


        #region Methods
        public void AddToSelection(User user)
        {
            _selectedUser = user;
        }

        public User GetSelected()
        {
            return _selectedUser;
        }

        public async Task Add(User user)
        {
            _authenticationService.Register(user.Email, user.PasswordHash);
            await Initialize();
            OnUserAdded(user);
        }

        public async Task Update(User user)
        {
            _authenticationService.Update(user);
            await Initialize();
            OnUserEdited(user);
        }

        public async Task Delete(User user)
        {
            _authenticationService.Delete(user);
            await Initialize();
            OnUserDeleted(user);
        }
        #endregion
        public User Login(User user)
        {
            User logUser = _authenticationService.Login(user.Email, user.PasswordHash);
            OnUserLogged(logUser); ;
            return logUser;
        }

        public void Logout()
        {
            //_accountService.Logout();
            _currentUser = null;
        }

        private void OnUserLogged(User user)
        {
            CurrentUserChanged?.Invoke(user);
        }

        #region Handle Event
        private void OnUserAdded(User user)
        {
            UserAdded?.Invoke(user);
        }

        private void OnUserEdited(User user)
        {
            UserEdited?.Invoke(user);
        }

        private void OnUserDeleted(User user)
        {
            UserDeleted?.Invoke(user);
        }
        #endregion
    }
}

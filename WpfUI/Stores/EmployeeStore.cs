﻿using Application.DTO;
using Application.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WpfUI.Stores
{
    public class EmployeeStore
    {
        private readonly List<EmployeeDTO> _employees;
        private readonly EmployeeService _employeeService;
        private EmployeeDTO _selectedEmployee;
        private Lazy<Task> _initializeLazy;

        public IEnumerable<EmployeeDTO> Employees => _employees;

        public event Action<EmployeeDTO> EmployeeAdded;
        public event Action<EmployeeDTO> EmployeeEdited;
        public event Action<EmployeeDTO> EmployeeDeleted;

        public EmployeeStore(EmployeeService employeeService)
        {
            _employeeService = employeeService;
            _initializeLazy = new Lazy<Task>(Initialize);
            _selectedEmployee = new();

            _employees = new List<EmployeeDTO>();
        }

        #region Init Data
        public async Task Load()
        {
            try
            {
                await _initializeLazy.Value;
            }
            catch (Exception)
            {
                _initializeLazy = new Lazy<Task>(Initialize);
                throw;
            }
        }
        public async Task Refresh() 
        {
            await Initialize();
        }

        private async Task Initialize()
        {
            IEnumerable<EmployeeDTO> employees = _employeeService.GetAll();

            _employees.Clear();
            _employees.AddRange(employees);
        }
        #endregion

        #region Methods
        public void AddToSelection(EmployeeDTO employee)
        {
            _selectedEmployee = employee;
        }

        public EmployeeDTO GetSelected()
        {
            return _selectedEmployee;
        }

        public async Task Add(EmployeeDTO employeeDTO)
        {
            _employeeService.Add(employeeDTO);
            await Initialize();
            OnEmployeeAdded(employeeDTO);
        }

        public async Task Update(EmployeeDTO employeeDTO)
        {
            _employeeService.Update(employeeDTO);
            await Initialize();
            OnEmployeeEdited(employeeDTO);
        }

        public async Task Delete(EmployeeDTO employeeDTO)
        {
            _employeeService.Delete(employeeDTO.Id);
            await Initialize();
            OnEmployeeDeleted(employeeDTO);
        }
        #endregion

        #region Handle Event
        private void OnEmployeeAdded(EmployeeDTO employee)
        {
            EmployeeAdded?.Invoke(employee);
        }

        private void OnEmployeeEdited(EmployeeDTO employee)
        {
            EmployeeEdited?.Invoke(employee);
        }

        private void OnEmployeeDeleted(EmployeeDTO employee)
        {
            EmployeeDeleted?.Invoke(employee);
        }
        #endregion
    }
}

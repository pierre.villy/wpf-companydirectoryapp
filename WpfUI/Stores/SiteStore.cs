﻿using Application.Services;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WpfUI.Stores
{
    public class SiteStore
    {
        private readonly List<Site> _sites;
        private Lazy<Task> _initializeLazy;
        private readonly SiteService _siteService;
        private Site _selectedSite;

        public IEnumerable<Site> Sites => _sites;

        public event Action<Site> SiteAdded;
        public event Action<Site> SiteEdited;
        public event Action<Site> SiteDeleted;

        //public event Action<Site> ReservationMade;

        public SiteStore(SiteService siteSite)
        {
            _siteService = siteSite;
            _initializeLazy = new Lazy<Task>(Initialize);
            _selectedSite = new();

            _sites = new List<Site>();
        }

        public async Task Load()
        {
            try
            {
                await _initializeLazy.Value;
            }
            catch (Exception)
            {
                _initializeLazy = new Lazy<Task>(Initialize);
                throw;
            }
        }

        public async Task Refresh()
        {
            await Initialize();
        }

        private async Task Initialize()
        {
            IEnumerable<Site> sites = _siteService.GetAll();

            _sites.Clear();
            _sites.AddRange(sites);
        }

        #region Methods

        public void AddToSelection(Site site)
        {
            _selectedSite = site;
        }

        public Site GetSelected()
        {
            return _selectedSite;
        }

        public async Task Add(Site site)
        {
            _siteService.Add(site);
            await Initialize();
            OnSiteAdded(site);
        }

        public async Task Update(Site site)
        {
            _siteService.Update(site);
            await Initialize();
            OnSiteEdited(site);
        }

        public async Task Delete(Site site)
        {
            _siteService.Delete(site.Id);
            await Initialize();
            OnSiteDeleted(site);
        }
        #endregion

        #region Handle Event
        private void OnSiteAdded(Site site)
        {
            SiteAdded?.Invoke(site);
        }

        private void OnSiteEdited(Site site)
        {
            SiteEdited?.Invoke(site);
        }

        private void OnSiteDeleted(Site site)
        {
            SiteDeleted?.Invoke(site);
        }
        #endregion
    }
}

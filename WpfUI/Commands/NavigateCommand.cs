﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfUI.Services.Navigation;
using WpfUI.Stores;

namespace WpfUI.Commands
{
    public class NavigateCommand : CommandBase
    {
        private readonly INavigationService _navigationService;
        private readonly NavigationStore _navigationStore;

        public NavigateCommand(INavigationService navigationService, NavigationStore navigationStore = null)
        {
            _navigationService = navigationService;
            _navigationStore = navigationStore;
        }

        public override void Execute(object parameter)
        {
            if (parameter != null)
            {
                var result = Enum.TryParse((string)parameter, out SelectedItemEnum myEnum);
                _navigationStore.SelectedItem = myEnum;
            }

            _navigationService.Navigate();
        }
    }
}

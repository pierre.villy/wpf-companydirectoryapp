﻿using Application.Interfaces;
using Application.Services;
using Domain.Interfaces;
using Infrastructure.Repositories;
using Microsoft.AspNet.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Windows;
using WpfUI.Services.Navigation;
using WpfUI.Stores;
using WpfUI.ViewModels;
using WpfUI.ViewModels.EmployeeViewModels;
using WpfUI.ViewModels.ServiceViewModels;
using WpfUI.ViewModels.SiteViewModels;
using WpfUI.ViewModels.UserViewModels;

namespace WpfUI
{
    public partial class App
    {
        private readonly IServiceProvider _serviceProvider;

        public App()
        {
            IServiceCollection services = new ServiceCollection();

            services.AddSingleton<AccountStore>();
            services.AddSingleton<EmployeeStore>();
            services.AddSingleton<ServiceStore>();
            services.AddSingleton<SiteStore>();
            services.AddSingleton<NavigationStore>();
            services.AddSingleton<ModalNavigationStore>();

            services.AddSingleton<INavigationService>(s => CreateHomeNavigationService(s));

            string connectionString = "Server=(localdb)\\MSSQLLocalDB;Database=myDataBase;Trusted_Connection=True;";
            //string connectionString = context.Configuration.GetConnectionString("Default");
            Action<DbContextOptionsBuilder> configureDbContext = o => o.UseSqlServer(connectionString);
            services.AddDbContext<CompanyDirectoryDbContext>(configureDbContext);
            services.AddSingleton<CompanyDirectoryDbContextFactory>(new CompanyDirectoryDbContextFactory(configureDbContext));

            services.AddScoped<CloseModalNavigationService>();
            services.AddScoped<IEmployeeRepository, EmployeeRepository>();
            services.AddScoped<IServiceRepository, ServiceRepository>();
            services.AddScoped<ISiteRepository, SiteRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<EmployeeService>();
            services.AddScoped<ServiceService>();
            services.AddScoped<SiteService>();
            services.AddScoped<AuthenticationService>();
            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddScoped<IPasswordHasher, PasswordHasher>();

            services.AddTransient<HomeViewModel>(s => new HomeViewModel(
                s.GetRequiredService<EmployeeStore>(),
                 s.GetRequiredService<ServiceStore>(),
                  s.GetRequiredService<SiteStore>()));

            services.AddTransient<EmployeeListViewModel>(s => new EmployeeListViewModel(
                s.GetRequiredService<EmployeeStore>()));

            services.AddTransient<AddEmployeeViewModel>(CreateAddEmployeeViewModel);
            services.AddTransient<EditEmployeeViewModel>(CreateEditEmployeeViewModel);
            services.AddTransient<DeleteEmployeeViewModel>(CreateDeleteEmployeeViewModel);

            services.AddTransient<AddServiceViewModel>(CreateAddServiceViewModel);
            services.AddTransient<EditServiceViewModel>(CreateEditServiceViewModel);
            services.AddTransient<DeleteServiceViewModel>(CreateDeleteServiceViewModel);

            services.AddTransient<AddSiteViewModel>(CreateAddSiteViewModel);
            services.AddTransient<EditSiteViewModel>(CreateEditSiteViewModel);
            services.AddTransient<DeleteSiteViewModel>(CreateDeleteSiteViewModel);

            services.AddTransient<AddUserViewModel>(CreateAddUserViewModel);
            services.AddTransient<EditUserViewModel>(CreateEditUserViewModel);
            services.AddTransient<DeleteUserViewModel>(CreateDeleteUserViewModel);


            services.AddTransient<ServiceListViewModel>(s => new ServiceListViewModel(
                s.GetRequiredService<ServiceStore>()));

            services.AddTransient<SiteListViewModel>(s => new SiteListViewModel(
                s.GetRequiredService<SiteStore>()));

            services.AddTransient<UserListViewModel>(s => new UserListViewModel(
                s.GetRequiredService<AccountStore>()));

            services.AddTransient<LoginViewModel>(CreateLoginViewModel);

            services.AddTransient<NavigationBarViewModel>(CreateNavigationBarViewModel);
            services.AddTransient<ToolBarViewModel>(CreateToolBarViewModel);

            services.AddSingleton<MainViewModel>();

            services.AddSingleton<MainWindow>(s => new MainWindow()
            {
                DataContext = s.GetRequiredService<MainViewModel>()
            });

            _serviceProvider = services.BuildServiceProvider();

        }

        protected override void OnStartup(StartupEventArgs e)
        {
            INavigationService initialNavigationService = _serviceProvider.GetRequiredService<INavigationService>();
            initialNavigationService.Navigate();

            MainWindow = _serviceProvider.GetRequiredService<MainWindow>();
            MainWindow.Show();

            base.OnStartup(e);
        }
        private INavigationService CreateHomeNavigationService(IServiceProvider serviceProvider)
        {
            return new LayoutNavigationService<HomeViewModel>(
                serviceProvider.GetRequiredService<NavigationStore>(),
                CreateHomeViewModel,
                () => serviceProvider.GetRequiredService<NavigationBarViewModel>(),
                () => serviceProvider.GetRequiredService<ToolBarViewModel>(),
                serviceProvider.GetRequiredService<AccountStore>());
        }
        private INavigationService CreateEmployeeNavigationService(IServiceProvider serviceProvider)
        {
            return new LayoutNavigationService<EmployeeListViewModel>(
                serviceProvider.GetRequiredService<NavigationStore>(),
                CreateEmployeeListViewModel,
                //() => serviceProvider.GetRequiredService<HomeViewModel>(),
                () => serviceProvider.GetRequiredService<NavigationBarViewModel>(),
                () => serviceProvider.GetRequiredService<ToolBarViewModel>(),
                serviceProvider.GetRequiredService<AccountStore>());
        }
        private INavigationService CreateServiceNavigationService(IServiceProvider serviceProvider)
        {
            return new LayoutNavigationService<ServiceListViewModel>(
                serviceProvider.GetRequiredService<NavigationStore>(),
                CreateServiceListViewModel,
                //() => serviceProvider.GetRequiredService<HomeViewModel>(),
                () => serviceProvider.GetRequiredService<NavigationBarViewModel>(),
                () => serviceProvider.GetRequiredService<ToolBarViewModel>(),
                serviceProvider.GetRequiredService<AccountStore>());
        }
        private INavigationService CreateSiteNavigationService(IServiceProvider serviceProvider)
        {
            return new LayoutNavigationService<SiteListViewModel>(
                serviceProvider.GetRequiredService<NavigationStore>(),
                CreateSiteListViewModel,
                //() => serviceProvider.GetRequiredService<HomeViewModel>(),
                () => serviceProvider.GetRequiredService<NavigationBarViewModel>(),
                () => serviceProvider.GetRequiredService<ToolBarViewModel>(),
                serviceProvider.GetRequiredService<AccountStore>());
        }
        private INavigationService CreateUserNavigationService(IServiceProvider serviceProvider)
        {
            return new LayoutNavigationService<UserListViewModel>(
                serviceProvider.GetRequiredService<NavigationStore>(),
                CreateUserListViewModel,
                () => serviceProvider.GetRequiredService<NavigationBarViewModel>(),
                () => serviceProvider.GetRequiredService<ToolBarViewModel>(),
                serviceProvider.GetRequiredService<AccountStore>());
        }
        private INavigationService CreateLoginNavigationService(IServiceProvider serviceProvider)
        {
            return new ModalNavigationService<LoginViewModel>(
                serviceProvider.GetRequiredService<ModalNavigationStore>(),
                () => serviceProvider.GetRequiredService<LoginViewModel>());
        }

        private NavigationBarViewModel CreateNavigationBarViewModel(IServiceProvider serviceProvider)
        {
            return new NavigationBarViewModel(serviceProvider.GetRequiredService<AccountStore>(),
                serviceProvider.GetRequiredService<NavigationStore>(),
                CreateHomeNavigationService(serviceProvider),
                CreateEmployeeNavigationService(serviceProvider),
                CreateServiceNavigationService(serviceProvider),
                CreateSiteNavigationService(serviceProvider),
                CreateUserNavigationService(serviceProvider),
                CreateLoginNavigationService(serviceProvider));
        }

        //ToolBar
        private ToolBarViewModel CreateToolBarViewModel(IServiceProvider serviceProvider)
        {
            return new ToolBarViewModel(serviceProvider.GetRequiredService<NavigationStore>(),
                CreateAddEmployeeNavigationService(serviceProvider),
                CreateEditEmployeeNavigationService(serviceProvider),
                CreateDeleteEmployeeNavigationService(serviceProvider),
                CreateAddServiceNavigationService(serviceProvider),
                CreateEditServiceNavigationService(serviceProvider),
                CreateDeleteServiceNavigationService(serviceProvider),
                CreateAddSiteNavigationService(serviceProvider),
                CreateEditSiteNavigationService(serviceProvider),
                CreateDeleteSiteNavigationService(serviceProvider),
                CreateAddUserNavigationService(serviceProvider),
                CreateEditUserNavigationService(serviceProvider),
                CreateDeleteUserNavigationService(serviceProvider));
        }

        #region ViewModels
        private HomeViewModel CreateHomeViewModel()
        {
            return HomeViewModel.LoadViewModel(
                _serviceProvider.GetRequiredService<EmployeeStore>(),
                _serviceProvider.GetRequiredService<ServiceStore>(),
                _serviceProvider.GetRequiredService<SiteStore>());
        }

        private EmployeeListViewModel CreateEmployeeListViewModel()
        {
            return EmployeeListViewModel.LoadViewModel(
                _serviceProvider.GetRequiredService<EmployeeStore>());
        }

        private ServiceListViewModel CreateServiceListViewModel()
        {
            return ServiceListViewModel.LoadViewModel(
                _serviceProvider.GetRequiredService<ServiceStore>());
        }

        private SiteListViewModel CreateSiteListViewModel()
        {
            return SiteListViewModel.LoadViewModel(
                _serviceProvider.GetRequiredService<SiteStore>());
        }

        private UserListViewModel CreateUserListViewModel()
        {
            return UserListViewModel.LoadViewModel(
                _serviceProvider.GetRequiredService<AccountStore>());
        }
        #endregion

        #region EmployeeNavigationServices
        private INavigationService CreateAddEmployeeNavigationService(IServiceProvider serviceProvider)
        {
            return new ModalNavigationService<AddEmployeeViewModel>(
                serviceProvider.GetRequiredService<ModalNavigationStore>(),
                () => serviceProvider.GetRequiredService<AddEmployeeViewModel>());
        }

        private INavigationService CreateEditEmployeeNavigationService(IServiceProvider serviceProvider)
        {
            return new ModalNavigationService<EditEmployeeViewModel>(
                serviceProvider.GetRequiredService<ModalNavigationStore>(),
                () => serviceProvider.GetRequiredService<EditEmployeeViewModel>());
        }

        private INavigationService CreateDeleteEmployeeNavigationService(IServiceProvider serviceProvider)
        {
            return new ModalNavigationService<DeleteEmployeeViewModel>(
                serviceProvider.GetRequiredService<ModalNavigationStore>(),
                () => serviceProvider.GetRequiredService<DeleteEmployeeViewModel>());
        }
        #endregion

        #region ServiceNavigationServices

        private INavigationService CreateAddServiceNavigationService(IServiceProvider serviceProvider)
        {
            return new ModalNavigationService<AddServiceViewModel>(
                serviceProvider.GetRequiredService<ModalNavigationStore>(),
                () => serviceProvider.GetRequiredService<AddServiceViewModel>());
        }

        private INavigationService CreateEditServiceNavigationService(IServiceProvider serviceProvider)
        {
            return new ModalNavigationService<EditServiceViewModel>(
                serviceProvider.GetRequiredService<ModalNavigationStore>(),
                () => serviceProvider.GetRequiredService<EditServiceViewModel>());
        }

        private INavigationService CreateDeleteServiceNavigationService(IServiceProvider serviceProvider)
        {
            return new ModalNavigationService<DeleteServiceViewModel>(
                serviceProvider.GetRequiredService<ModalNavigationStore>(),
                () => serviceProvider.GetRequiredService<DeleteServiceViewModel>());
        }
        #endregion

        #region SiteNavigationServices
        private INavigationService CreateAddSiteNavigationService(IServiceProvider serviceProvider)
        {
            return new ModalNavigationService<AddSiteViewModel>(
                serviceProvider.GetRequiredService<ModalNavigationStore>(),
                () => serviceProvider.GetRequiredService<AddSiteViewModel>());
        }

        private INavigationService CreateEditSiteNavigationService(IServiceProvider serviceProvider)
        {
            return new ModalNavigationService<EditSiteViewModel>(
                serviceProvider.GetRequiredService<ModalNavigationStore>(),
                () => serviceProvider.GetRequiredService<EditSiteViewModel>());
        }

        private INavigationService CreateDeleteSiteNavigationService(IServiceProvider serviceProvider)
        {
            return new ModalNavigationService<DeleteSiteViewModel>(
                serviceProvider.GetRequiredService<ModalNavigationStore>(),
                () => serviceProvider.GetRequiredService<DeleteSiteViewModel>());
        }
        #endregion

        #region UserNavigationServices
        private INavigationService CreateAddUserNavigationService(IServiceProvider serviceProvider)
        {
            return new ModalNavigationService<AddUserViewModel>(
                serviceProvider.GetRequiredService<ModalNavigationStore>(),
                () => serviceProvider.GetRequiredService<AddUserViewModel>());
        }

        private INavigationService CreateEditUserNavigationService(IServiceProvider serviceProvider)
        {
            return new ModalNavigationService<EditUserViewModel>(
                serviceProvider.GetRequiredService<ModalNavigationStore>(),
                () => serviceProvider.GetRequiredService<EditUserViewModel>());
        }

        private INavigationService CreateDeleteUserNavigationService(IServiceProvider serviceProvider)
        {
            return new ModalNavigationService<DeleteUserViewModel>(
                serviceProvider.GetRequiredService<ModalNavigationStore>(),
                () => serviceProvider.GetRequiredService<DeleteUserViewModel>());
        }
        #endregion

        private LoginViewModel CreateLoginViewModel(IServiceProvider serviceProvider)
        {
            CompositeNavigationService navigationService = new CompositeNavigationService(
                serviceProvider.GetRequiredService<CloseModalNavigationService>(),
                CreateHomeNavigationService(serviceProvider));

            return new LoginViewModel(
                serviceProvider.GetRequiredService<AccountStore>(),
                navigationService);
        }

        #region Employee Components
        private AddEmployeeViewModel CreateAddEmployeeViewModel(IServiceProvider serviceProvider)
        {
            CompositeNavigationService navigationService = new CompositeNavigationService(
                serviceProvider.GetRequiredService<CloseModalNavigationService>(),
                CreateEmployeeNavigationService(serviceProvider));

            return new AddEmployeeViewModel(
                serviceProvider.GetRequiredService<EmployeeStore>(),
                serviceProvider.GetRequiredService<ServiceStore>(),
                serviceProvider.GetRequiredService<SiteStore>(),
                navigationService);
        }

        private EditEmployeeViewModel CreateEditEmployeeViewModel(IServiceProvider serviceProvider)
        {
            CompositeNavigationService navigationService = new CompositeNavigationService(
                serviceProvider.GetRequiredService<CloseModalNavigationService>(),
                CreateEmployeeNavigationService(serviceProvider));

            return new EditEmployeeViewModel(
                serviceProvider.GetRequiredService<EmployeeStore>(),
                serviceProvider.GetRequiredService<ServiceStore>(),
                serviceProvider.GetRequiredService<SiteStore>(),
                navigationService);
        }

        private DeleteEmployeeViewModel CreateDeleteEmployeeViewModel(IServiceProvider serviceProvider)
        {
            CompositeNavigationService navigationService = new CompositeNavigationService(
                serviceProvider.GetRequiredService<CloseModalNavigationService>(),
                CreateEmployeeNavigationService(serviceProvider));

            return new DeleteEmployeeViewModel(
                serviceProvider.GetRequiredService<EmployeeStore>(),
                serviceProvider.GetRequiredService<ServiceStore>(),
                serviceProvider.GetRequiredService<SiteStore>(),
                navigationService);
        }
        #endregion

        #region Service Components
        private AddServiceViewModel CreateAddServiceViewModel(IServiceProvider serviceProvider)
        {
            CompositeNavigationService navigationService = new CompositeNavigationService(
                serviceProvider.GetRequiredService<CloseModalNavigationService>(),
                CreateServiceNavigationService(serviceProvider));

            return new AddServiceViewModel(
                serviceProvider.GetRequiredService<ServiceStore>(),
                navigationService);
        }

        private EditServiceViewModel CreateEditServiceViewModel(IServiceProvider serviceProvider)
        {
            CompositeNavigationService navigationService = new CompositeNavigationService(
                serviceProvider.GetRequiredService<CloseModalNavigationService>(),
                CreateServiceNavigationService(serviceProvider));

            return new EditServiceViewModel(
                serviceProvider.GetRequiredService<ServiceStore>(),
                navigationService,
                serviceProvider.GetRequiredService<EmployeeStore>());
        }

        private DeleteServiceViewModel CreateDeleteServiceViewModel(IServiceProvider serviceProvider)
        {
            CompositeNavigationService navigationService = new CompositeNavigationService(
                serviceProvider.GetRequiredService<CloseModalNavigationService>(),
                CreateServiceNavigationService(serviceProvider));

            return new DeleteServiceViewModel(
                serviceProvider.GetRequiredService<ServiceStore>(),
                navigationService);
        }
        #endregion

        #region Site Components
        private AddSiteViewModel CreateAddSiteViewModel(IServiceProvider serviceProvider)
        {
            CompositeNavigationService navigationService = new CompositeNavigationService(
                serviceProvider.GetRequiredService<CloseModalNavigationService>(),
                CreateSiteNavigationService(serviceProvider));

            return new AddSiteViewModel(
                serviceProvider.GetRequiredService<SiteStore>(),
                navigationService);
        }

        private EditSiteViewModel CreateEditSiteViewModel(IServiceProvider serviceProvider)
        {
            CompositeNavigationService navigationService = new CompositeNavigationService(
                serviceProvider.GetRequiredService<CloseModalNavigationService>(),
                CreateSiteNavigationService(serviceProvider));

            return new EditSiteViewModel(
                serviceProvider.GetRequiredService<SiteStore>(),
                navigationService,
                serviceProvider.GetRequiredService<ServiceStore>(),
                serviceProvider.GetRequiredService<EmployeeStore>());
        }

        private DeleteSiteViewModel CreateDeleteSiteViewModel(IServiceProvider serviceProvider)
        {
            CompositeNavigationService navigationService = new CompositeNavigationService(
                serviceProvider.GetRequiredService<CloseModalNavigationService>(),
                CreateSiteNavigationService(serviceProvider));

            return new DeleteSiteViewModel(
                serviceProvider.GetRequiredService<SiteStore>(),
                navigationService);
        }
        #endregion

        #region User Components
        private AddUserViewModel CreateAddUserViewModel(IServiceProvider serviceProvider)
        {
            CompositeNavigationService navigationService = new CompositeNavigationService(
                serviceProvider.GetRequiredService<CloseModalNavigationService>(),
                CreateUserNavigationService(serviceProvider));

            return new AddUserViewModel(
                serviceProvider.GetRequiredService<AccountStore>(),
                navigationService);
        }

        private EditUserViewModel CreateEditUserViewModel(IServiceProvider serviceProvider)
        {
            CompositeNavigationService navigationService = new CompositeNavigationService(
                serviceProvider.GetRequiredService<CloseModalNavigationService>(),
                CreateUserNavigationService(serviceProvider));

            return new EditUserViewModel(
                serviceProvider.GetRequiredService<AccountStore>(),
                navigationService);
        }

        private DeleteUserViewModel CreateDeleteUserViewModel(IServiceProvider serviceProvider)
        {
            CompositeNavigationService navigationService = new CompositeNavigationService(
                serviceProvider.GetRequiredService<CloseModalNavigationService>(),
                CreateUserNavigationService(serviceProvider));

            return new DeleteUserViewModel(
                serviceProvider.GetRequiredService<AccountStore>(),
                navigationService);
        }
        #endregion
    }
}

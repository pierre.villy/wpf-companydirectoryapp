﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace WpfUI.HostBuilders
{
    public static class AddConfigurationHostBuilderExtensions
    {
        public static IHostBuilder AddConfiguration(this IHostBuilder host)
        {
            host.ConfigureAppConfiguration(c =>
            {
                c.AddJsonFile("C:\\Users\\pierr\\source\\repos\\CompanyDirectoryApp\\WpfUI\\appsettings.json");
                c.AddEnvironmentVariables();
            });

            return host;
        }
    }
}

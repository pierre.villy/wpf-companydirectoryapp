﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WpfUI.Stores;
using WpfUI.ViewModels;

namespace WpfUI.HostBuilders
{
    public static class AddViewsHostBuilderExtensions
    {
        public static IHostBuilder AddViews(this IHostBuilder host)
        {
            host.ConfigureServices(services =>
            {
                //services.AddSingleton<MainWindow>(s => new MainWindow(s.GetRequiredService<MainViewModel>()));
                services.AddSingleton<HomeViewModel>(s => new HomeViewModel(s.GetRequiredService<EmployeeStore>(),
                s.GetRequiredService<ServiceStore>(),s.GetRequiredService<SiteStore>()));
            });

            return host;
        }
    }
}

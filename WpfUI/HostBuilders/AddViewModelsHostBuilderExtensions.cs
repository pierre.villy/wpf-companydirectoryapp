﻿using Application.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using WpfUI.Services.Navigation;
using WpfUI.Stores;
using WpfUI.ViewModels;

namespace WpfUI.HostBuilders
{
    public static class AddViewModelsHostBuilderExtensions
    {
        public static IHostBuilder AddViewModels(this IHostBuilder hostBuilder)
        {
            hostBuilder.ConfigureServices(services =>
            {
                services.AddTransient<MainViewModel>();
                services.AddSingleton<HomeViewModel>(s => new HomeViewModel(s.GetRequiredService<EmployeeStore>(),
                    s.GetRequiredService<ServiceStore>(), s.GetRequiredService<SiteStore>()));


                //services.AddTransient((s) => CreateReservationListingViewModel(s));
                //services.AddSingleton<Func<ReservationListingViewModel>>((s) => () => s.GetRequiredService<ReservationListingViewModel>());
                //services.AddSingleton<NavigationService<ReservationListingViewModel>>();

                //services.AddTransient<MakeReservationViewModel>();
                //services.AddSingleton<Func<MakeReservationViewModel>>((s) => () => s.GetRequiredService<MakeReservationViewModel>());
                //services.AddSingleton<NavigationService<MakeReservationViewModel>>();


            });

            return hostBuilder;
        }

        private static HomeViewModel CreateHomeViewModel(IServiceProvider services)
        {
            return HomeViewModel.LoadViewModel(
                services.GetRequiredService<EmployeeStore>(),
                services.GetRequiredService<ServiceStore>(),
                services.GetRequiredService<SiteStore>());
        }

        //private static MainViewModel CreateMainViewModel(IServiceProvider services)
        //{
        //    return MainViewModel.LoadViewModel(
        //        services.GetRequiredService<EmployeeStore>());
        //}



        //private static ReservationListingViewModel CreateReservationListingViewModel(IServiceProvider services)
        //{
        //    return ReservationListingViewModel.LoadViewModel(
        //        services.GetRequiredService<HotelStore>(),
        //        services.GetRequiredService<NavigationService<MakeReservationViewModel>>());
        //}
    }
}

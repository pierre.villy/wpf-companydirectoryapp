﻿using Application.Services;
using Domain.Entities;
using Domain.Interfaces;
using Infrastructure.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;
using WpfUI.Services.Navigation;

namespace WpfUI.HostBuilders
{
    public static class AddServicesHostBuilderExtensions
    {
        public static IHostBuilder AddServices(this IHostBuilder host)
        {
            host.ConfigureServices(services =>
            {
                //services.AddSingleton<IUnitOfWork>();
                services.AddSingleton<INavigationService>();

                services.AddSingleton<IUnitOfWork, UnitOfWork>();
                services.AddSingleton<IEmployeeRepository, EmployeeRepository>();
                services.AddSingleton<EmployeeService>();
                //services.AddSingleton<IPasswordHasher, PasswordHasher>();

                //services.AddSingleton<IAuthenticationService, AuthenticationService>();
                //services.AddSingleton<IDataService<Account>, AccountDataService>();
                //services.AddSingleton<IAccountService, AccountDataService>();
                //services.AddSingleton<IStockPriceService, StockPriceService>();
                //services.AddSingleton<IBuyStockService, BuyStockService>();
                //services.AddSingleton<ISellStockService, SellStockService>();
                //services.AddSingleton<IMajorIndexService, MajorIndexService>();
            });

            return host;
        }
    }
}

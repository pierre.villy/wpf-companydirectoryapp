# Projet étudiant WPF

Le but du projet était de réaliser une application client lourd de type "Annuaire d'entreprise"

Voir la vidéo de démo : https://gitlab.com/pierre.villy/wpf-companydirectoryapp/-/blob/master/Demo/demo_wpf_app.mp4

## Fonctionnalités 
En tant que visiteur : 
- Afficher la liste des employés
- Filtrer la liste via la recherche dynamique
- Filtrer la liste par Site ou service
- Réinitialiser les filtres

En tant qu'Admin : Se connecter en cliquant sur CTRL+R
- Ajouter, modifier, supprimer : Employé, Site, Service, Utilisateurs
- Menu Global et Menu contextuel pour chaque liste du CRUD

﻿namespace Domain.Entities
{
    public class Employee
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string CellPhone { get; set; }
        public string ImagePath { get; set; }

        public int SiteId { get; set; }
        public Site Site { get; set; }
        public int ServiceId { get; set; }
        public Service Service { get; set; }
    }
}

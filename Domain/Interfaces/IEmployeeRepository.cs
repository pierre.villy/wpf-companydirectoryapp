﻿using Domain.Entities;

namespace Domain.Interfaces
{
    public interface IEmployeeRepository : IRepositoryBase<Employee>
    {
    }
}

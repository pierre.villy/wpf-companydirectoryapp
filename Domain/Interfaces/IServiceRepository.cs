﻿using Domain.Entities;

namespace Domain.Interfaces
{
    public interface IServiceRepository : IRepositoryBase<Service>
    {
    }
}

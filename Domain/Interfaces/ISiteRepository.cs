﻿using Domain.Entities;

namespace Domain.Interfaces
{
    public interface ISiteRepository : IRepositoryBase<Site>
    {
    }
}

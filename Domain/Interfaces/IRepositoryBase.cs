﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Domain.Interfaces
{
    public interface IRepositoryBase<T>
    {
        /// <summary>
        /// SELECT all object from the table
        /// </summary>
        /// <returns></returns>
        List<T> GetAll();

        /// <summary>
        /// SELECT object from table corresponding to conditions. If several result, return the first row.
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        T GetOneByCondition(Expression<Func<T, bool>> expression);

        /// <summary>
        /// SELECT objects from table corresponding to certains conditions.
        /// This example provide all objects from article table which the descrtion field equal to "coktail" :
        /// <example>
        /// <code>
        /// UnitOfWork.ArticleRepository.GetByCondition(article => article.description == "coktail");
        /// </code>
        /// </example>
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        List<T> GetManyByCondition(Expression<Func<T, bool>> expression);

        /// <summary>
        /// INSERT into table the object passed into parameter
        /// </summary>
        /// <param name="entity"></param>
        T Create(T entity);

        /// <summary>
        /// UPDATE the object with the same primary key than the object passed into parameter in the database
        /// </summary>
        /// <param name="entity"></param>
        void Update(T entity);

        /// <summary>
        /// DELETE from table the object with the same primary key than the object  passed into parameter
        /// </summary>
        /// <param name="entity"></param>
        void Delete(T entity);
    }
}

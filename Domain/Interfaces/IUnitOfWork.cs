﻿namespace Domain.Interfaces
{
    public interface IUnitOfWork
    {
        IEmployeeRepository EmployeeRepository { get; }
        IServiceRepository ServiceRepository { get; }
        ISiteRepository SiteRepository { get; }
        IUserRepository UserRepository { get; }

        void SaveChanges();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IDataBaseHelper
    {
        public void Seed();
        public void DropDatabase();
        public void CreateDatabase();
        public void CreateUsers();
    }
}

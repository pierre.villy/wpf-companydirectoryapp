﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IAuthenticationService
    {
        public enum RegistrationResult
        {
            Success,
            PasswordsDoNotMatch,
            EmailAlreadyExists,
            UsernameAlreadyExists
        }

        ///// <summary>
        ///// Register a new user.
        ///// </summary>
        ///// <param name="email">The user's email.</param>
        ///// <param name="password">The user's password.</param>
        ///// <param name="confirmPassword">The user's confirmed password.</param>
        ///// <returns>The result of the registration.</returns>
        ///// <exception cref="Exception">Thrown if the registration fails.</exception>
        RegistrationResult Register(string email, string password);

        /// <summary>
        /// Get an account for a user's credentials.
        /// </summary>
        /// <param name="email">The user's email.</param>
        /// <param name="password">The user's password.</param>
        /// <returns>The account for the user.</returns>
        /// <exception cref="UserNotFoundException">Thrown if the user does not exist.</exception>
        /// <exception cref="InvalidPasswordException">Thrown if the password is invalid.</exception>
        /// <exception cref="Exception">Thrown if the login fails.</exception>
        User Login(string email, string password);

        List<User> GetAll();
        User GetOneById(int id);
        void Update(User user);
        void Delete(User user);
    }
}

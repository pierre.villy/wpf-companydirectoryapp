﻿using Application.DTO;
using Domain.Entities;
using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class SiteService
    {
        private readonly IUnitOfWork _unitOfWork;

        public SiteService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public List<Site> GetAll()
        {
            return _unitOfWork.SiteRepository.GetAll().OrderBy(s => s.Name).ToList();
        }

        public List<Site> GetAllSitesWithServices()
        {
            List<Site> sites = _unitOfWork.SiteRepository.GetAll();
            return sites;
        }

        public Site GetOneById(int id)
        {
            return _unitOfWork.SiteRepository.GetOneByCondition(s => s.Id == id);
        }

        public Site Add(Site site)
        {
            _unitOfWork.SiteRepository.Create(site);
            _unitOfWork.SaveChanges();
            return site;
        }

        public void Update(Site site)
        {
            _unitOfWork.SiteRepository.Update(site);
            _unitOfWork.SaveChanges();
        }

        public void Delete(int id)
        {
            Site site = _unitOfWork.SiteRepository.GetOneByCondition(s => s.Id == id);
            _unitOfWork.SiteRepository.Delete(site);
            _unitOfWork.SaveChanges();
        }
    }
}

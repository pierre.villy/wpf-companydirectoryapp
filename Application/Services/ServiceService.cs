﻿using Application.DTO;
using Domain.Entities;
using Domain.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Application.Services
{
    public class ServiceService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ServiceService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public List<ServiceDTO> GetAll()
        {
            return _unitOfWork.ServiceRepository.GetAll().Select(s => new ServiceDTO()
            {
                Id = s.Id,
                Name = s.Name,
            }).OrderBy(s => s.Name).ToList();
        }

        public Service GetOneById(int id)
        {
            return _unitOfWork.ServiceRepository.GetOneByCondition(s => s.Id == id);
        }

        public Service Add(ServiceDTO serviceDto)
        {
            var service = new Service()
            {
                Name = serviceDto.Name,
            };
            _unitOfWork.ServiceRepository.Create(service);
            _unitOfWork.SaveChanges();
            return service;
        }

        public void Update(ServiceDTO serviceDto)
        {
            var service = new Service()
            {
                Id = serviceDto.Id,
                Name = serviceDto.Name,
            };
            _unitOfWork.ServiceRepository.Update(service);
            _unitOfWork.SaveChanges();
        }

        public void Delete(int id)
        {
            Service service = _unitOfWork.ServiceRepository.GetOneByCondition(s => s.Id == id);
            _unitOfWork.ServiceRepository.Delete(service);
            _unitOfWork.SaveChanges();
        }
    }
}

﻿using Application.DTO;
using Domain.Entities;
using Domain.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Application.Services
{
    public class EmployeeService
    {
        private readonly IUnitOfWork _unitOfWork;

        public EmployeeService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public List<EmployeeDTO> GetAll()
        {
            return _unitOfWork.EmployeeRepository.GetAll().Select(e => new EmployeeDTO()
            {
                Id = e.Id,
                FirstName = e.FirstName,
                LastName = e.LastName,
                Phone = e.Phone,
                CellPhone = e.CellPhone,
                Email = e.Email,
                ServiceId = e.ServiceId,
                ServiceName = _unitOfWork.ServiceRepository.GetOneByCondition(s => s.Id == e.ServiceId).Name,
                SiteId = e.SiteId,
                SiteName = _unitOfWork.SiteRepository.GetOneByCondition(s => s.Id == e.SiteId).Name,
                ImagePath = e.ImagePath,
            }).ToList();
        }

        public Employee GetOneById(int id)
        {
            return _unitOfWork.EmployeeRepository.GetOneByCondition(e => e.Id == id);
        }

        public EmployeeDTO Add(EmployeeDTO employeeDto)
        {
            var employee = new Employee(){
                Id = employeeDto.Id,
                FirstName = employeeDto.FirstName,
                LastName = employeeDto.LastName,
                Phone = employeeDto.Phone,
                CellPhone = employeeDto.CellPhone,
                Email = employeeDto.Email,
                ServiceId = employeeDto.ServiceId,
                SiteId = employeeDto.SiteId,
                ImagePath = employeeDto.ImagePath
            };
            _unitOfWork.EmployeeRepository.Create(employee);
            _unitOfWork.SaveChanges();
            return employeeDto;
        }

        public void Update(EmployeeDTO employeeDto)
        {
            var employee = new Employee()
            {
                Id = employeeDto.Id,
                FirstName = employeeDto.FirstName,
                LastName = employeeDto.LastName,
                Phone = employeeDto.Phone,
                CellPhone = employeeDto.CellPhone,
                Email = employeeDto.Email,
                ServiceId = employeeDto.ServiceId,
                SiteId = employeeDto.SiteId,
                ImagePath = employeeDto.ImagePath
            };
            _unitOfWork.EmployeeRepository.Update(employee);
            _unitOfWork.SaveChanges();

        }

        public void Delete(int id)
        {
            Employee employee = _unitOfWork.EmployeeRepository.GetOneByCondition(e => e.Id == id);
            _unitOfWork.EmployeeRepository.Delete(employee);
            _unitOfWork.SaveChanges();
        }
    }
}

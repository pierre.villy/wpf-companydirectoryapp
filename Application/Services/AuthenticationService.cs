﻿using Application.Interfaces;
using Domain.Entities;
using Domain.Interfaces;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using static Application.Interfaces.IAuthenticationService;

namespace Application.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPasswordHasher _passwordHasher;

        public AuthenticationService(IPasswordHasher passwordHasher, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _passwordHasher = passwordHasher;
        }

        public List<User> GetAll()
        {
            return _unitOfWork.UserRepository.GetAll();
        }

        public User GetOneById(int id)
        {
            return _unitOfWork.UserRepository.GetOneByCondition(u => u.Id.Equals(id));
        }

        public void Update(User user)
        {
            _unitOfWork.UserRepository.Update(user);
            _unitOfWork.SaveChanges();
        }

        public void Delete(User user)
        {
            _unitOfWork.UserRepository.Delete(user);
            _unitOfWork.SaveChanges();
        }

        public User Login(string email, string password)
        {
            //Account storedAccount = await _accountService.GetByUsername(username);

            User user = _unitOfWork.UserRepository.GetOneByCondition(u => u.Email == email);

            //if (storedAccount == null)
            //{
            //    throw new UserNotFoundException(username);
            //}

            PasswordVerificationResult passwordResult = _passwordHasher.VerifyHashedPassword(user.PasswordHash, password);

            if (passwordResult != PasswordVerificationResult.Success)
            {
                throw new Exception();
            }

            return user;
        }

        public RegistrationResult Register(string email, string password)
        {
            RegistrationResult result = RegistrationResult.Success;

            //if (password != confirmPassword)
            //{
            //    result = RegistrationResult.PasswordsDoNotMatch;
            //}

            List<User> users = _unitOfWork.UserRepository.GetAll();
            for(int i = 0; i < users.Count; i++)
            {
                if(users[i].Email == email)
                    result = RegistrationResult.EmailAlreadyExists;
            }

            //User userEmail = _unitOfWork.UserRepository.GetOneByCondition(u => u.Email == email);
            //if (userEmail != null)
            //{
            //    result = RegistrationResult.EmailAlreadyExists;
            //}
            //Employee employeeEmail = _unitOfWork.EmployeeRepository.GetOneByCondition(e => e.Email.Equals(email));
            //if (employeeEmail != null)
            //{
            //    result = RegistrationResult.Success;
            //}

            if (result == RegistrationResult.Success)
            {
                string hashedPassword = _passwordHasher.HashPassword(password);

                User user = new User()
                {
                    Email = email,
                    PasswordHash = hashedPassword,
                    CreatedDate = DateTime.Now,
                    IsAdmin = true
                    
                };

                _unitOfWork.UserRepository.Create(user);
                _unitOfWork.SaveChanges();
            }

            return result;
        }
    }
}

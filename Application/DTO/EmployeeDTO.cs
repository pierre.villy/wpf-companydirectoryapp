﻿namespace Application.DTO
{
    public class EmployeeDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string CellPhone { get; set; }
        public string Email { get; set; }
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public int SiteId { get; set; }
        public string SiteName { get; set; }
        public string ImagePath { get; set; }
        public bool ImageIsNotNull => ImagePath != null;
        public string FullName => $"{FirstName} {LastName}";
    }
}

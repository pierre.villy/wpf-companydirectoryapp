﻿using Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Infrastructure.Repositories
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        protected CompanyDirectoryDbContext DbContext { get; set; }

        public RepositoryBase(CompanyDirectoryDbContext dbContext)
        {
            DbContext = dbContext;
        }


        public T Create(T entity)
        {
            DbContext.Set<T>().Add(entity);
            return entity;
        }

        public void Delete(T entity)
        {
            DbContext.Set<T>().Remove(entity);
        }

        public virtual List<T> GetAll()
        {
            return DbContext.Set<T>().AsNoTracking().ToList();
        }

        public virtual List<T> GetManyByCondition(Expression<Func<T, bool>> expression)
        {
            return DbContext.Set<T>().Where(expression).AsNoTracking().ToList();
        }

        public virtual T GetOneByCondition(Expression<Func<T, bool>> expression)
        {
            T value = DbContext.Set<T>().Where(expression).FirstOrDefault();
            DbContext.Entry<T>(value).State = EntityState.Detached;
            return value;
        }

        public void Update(T entity)
        {
            DbContext.Set<T>().Update(entity);
        }
    }
}

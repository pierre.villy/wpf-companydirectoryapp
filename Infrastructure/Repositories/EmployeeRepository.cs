﻿using Domain.Entities;
using Domain.Interfaces;

namespace Infrastructure.Repositories
{
    public class EmployeeRepository : RepositoryBase<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(CompanyDirectoryDbContext dbContext) : base(dbContext)
        {

        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using System;

namespace Infrastructure.Repositories
{
    public class CompanyDirectoryDbContextFactory
    {
        private readonly Action<DbContextOptionsBuilder> _configureDbContext;

        public CompanyDirectoryDbContextFactory(Action<DbContextOptionsBuilder> configureDbContext)
        {
            _configureDbContext = configureDbContext;
        }

        public CompanyDirectoryDbContext CreateDbContext()
        {
            DbContextOptionsBuilder<CompanyDirectoryDbContext> options = new DbContextOptionsBuilder<CompanyDirectoryDbContext>();

            _configureDbContext(options);

            return new CompanyDirectoryDbContext(options.Options);
        }
    }
}

﻿using Domain.Entities;
using Domain.Interfaces;

namespace Infrastructure.Repositories
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(CompanyDirectoryDbContext dbContext) : base(dbContext)
        {

        }
    }
}

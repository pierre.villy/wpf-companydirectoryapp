﻿using Domain.Interfaces;

namespace Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CompanyDirectoryDbContext _dbContext;

        public UnitOfWork(CompanyDirectoryDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        private IEmployeeRepository _employeeRepository;
        private IServiceRepository _serviceRepository;
        private ISiteRepository _siteRepository;
        private IUserRepository _userRepository;

        public IEmployeeRepository EmployeeRepository
        {
            get
            {
                _employeeRepository ??= new EmployeeRepository(_dbContext);
                return _employeeRepository;
            }
            set { _employeeRepository = value; }
        }

        public IServiceRepository ServiceRepository
        {
            get
            {
                _serviceRepository ??= new ServiceRepository(_dbContext);
                return _serviceRepository;
            }
            set { _serviceRepository = value; }
        }

        public ISiteRepository SiteRepository
        {
            get
            {
                _siteRepository ??= new SiteRepository(_dbContext);
                return _siteRepository;
            }
            set { _siteRepository = value; }
        }

        public IUserRepository UserRepository
        {
            get
            {
                _userRepository ??= new UserRepository(_dbContext);
                return _userRepository;
            }
            set { _userRepository = value; }
        }

        /// <summary>
        /// Method called to COMMIT all changes performed within the DB
        /// </summary>
        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Infrastructure.Repositories
{
    public  class CompanyDirectoryDesignTimeDbContextFactory : IDesignTimeDbContextFactory<CompanyDirectoryDbContext>
    {
        public CompanyDirectoryDbContext CreateDbContext(string[] args)
        {
            DbContextOptions options = new DbContextOptionsBuilder().UseSqlServer("Server=(localdb)\\MSSQLLocalDB;Database=myDataBase;Trusted_Connection=True;").Options;

            return new CompanyDirectoryDbContext(options);
        }
    }
}

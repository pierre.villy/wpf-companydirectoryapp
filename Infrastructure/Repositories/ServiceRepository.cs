﻿using Domain.Entities;
using Domain.Interfaces;

namespace Infrastructure.Repositories
{
    public class ServiceRepository : RepositoryBase<Service>, IServiceRepository
    {
        public ServiceRepository(CompanyDirectoryDbContext dbContext) : base(dbContext)
        {

        }
    }
}

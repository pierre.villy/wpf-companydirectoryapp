﻿using Domain.Entities;
using Domain.Interfaces;

namespace Infrastructure.Repositories
{
    public class SiteRepository : RepositoryBase<Site>, ISiteRepository
    {
        public SiteRepository(CompanyDirectoryDbContext dbContext) : base(dbContext)
        {

        }
    }
}

﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class CompanyDirectoryDbContext : DbContext
    {
        public CompanyDirectoryDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<Site> Sites { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region Employee constraints

            modelBuilder.Entity<Employee>(prop =>
            {
                prop.Property(e => e.Id)
                    .ValueGeneratedOnAdd().UseIdentityColumn();
                prop.Property(e => e.FirstName)
                    .IsRequired();
                prop.Property(e => e.LastName)
                    .IsRequired();
                prop.Property(e => e.Email)
                    .IsRequired();
                prop.Property(e => e.Phone)
                    .IsRequired();
                prop.Property(e => e.CellPhone)
                    .IsRequired();
                prop.Property(e => e.ImagePath)
                    .IsRequired(false);
                prop.Property(e => e.SiteId)
                    .IsRequired();
                prop.Property(e => e.ServiceId)
                    .IsRequired();

                //prop.HasAlternateKey(e => new { e.SiteId, e.ServiceId });

                prop.HasOne<Site>(e => e.Site)
                .WithMany(s => s.Employees)
                .HasForeignKey(e => e.SiteId);

                prop.HasOne<Service>(e => e.Service)
                .WithMany(s => s.Employees)
                .HasForeignKey(e => e.ServiceId);
                
            });

            #endregion

            #region Service constraints

            modelBuilder.Entity<Service>(prop =>
            {
                prop.Property(s => s.Id)
                    .ValueGeneratedOnAdd();

                prop.Property(s => s.Name)
                    .IsRequired();
            });

            #endregion

            #region Site constraints

            modelBuilder.Entity<Site>(prop =>
            {
                prop.Property(s => s.Id)
                     .ValueGeneratedOnAdd();
                prop.Property(s => s.Name)
                    .IsRequired();

            });

            #endregion

            #region User constraints

            modelBuilder.Entity<User>(prop =>
            {
                prop.Property(u => u.Id)
                     .ValueGeneratedOnAdd();
                prop.Property(u => u.Email)
                    .IsRequired();
                prop.Property(u => u.CreatedDate)
                    .HasDefaultValueSql("getdate()");
            });

            #endregion
        }
    }
}
